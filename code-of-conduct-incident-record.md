# Code of Conduct incident record

TIPS FOR THE COMMUNITY MANAGER:
When gathering information from an incident reporter, strive for these goals:

* **Safety** - Bring in another community manager if something about the situation is unsafe or dangerous, but avoid inviting too many community managers into the discussion. You might make the incident reporter feel overwhelmed if there are too many community manager.
* **Privacy** - Where possible, always strive to uphold the incident reporter's privacy. Obtain permission to proceed with the investigation.
* **Empathy** - Be an understanding, active listener who recognizes the real emotions being felt by the incident reporter. For example, say: "It sounds like you felt (this emotion) when (this thing happened)."
* **Support** - You should default to believing the reporter. Ask if there's anything you can do to make the incident reporter feel safe, emotionally whole, or restored. Explain the possible outcomes that are available, as provided in the [Code of Conduct](CODE_OF_CONDUCT.md) (correction, warning, temporary ban, permanent ban). However, avoid making any direct promises for exactly how the report will be handled until the investigation is concluded.
* **Acknowledgment** - At the end of the meeting, thank the incident reporter for being part of the community and for reaching out about the incident. Let the reporter know that you will be in touch to explain how the incident will be resolved after the investigation is complete.


## Incident number

{Assign a number to this incident for tracking purposes.
It can include the date an investigation was opened.
For example, yyyy-001.}


## Name of community manager who took the report

{Put your name here.}


## Name of incident reporter

{If the reporter wants to be anonymous, include a simple description of their role or involvement in the project.}


## Reporter's contact information (optional)

{Ask the reporter if they would like to provide their name and contact information.
If yes, fill in the contact information.}


## Permission from incident reporter to proceed?

{Yes/no.} {If no, ask if they would like to hold the report in escrow.}


## Date, time, and location of incident (optional)

{yyyy-mm-dd} {time} {location}.


## Additional witnesses or contacts (optional)

{List anyone who might be able to provide additional information as part of the investigation.}


## Incident description

{Only document information required to inform the report resolution.
Where possible, avoid documenting your opinion about the incident, or any information about individuals that is not relevant to the report.}
