## Summary

> Brief description of your request.

## Scoring

### Impact

> How will this change impact the target community?

### Reach

> How many people or organizations will be impacted by this change?

### Community/Goodwill

> How will this change influence the sense of Community and/or improve Goodwill for the project?

### Confidence

> How confident are you in the ability of the project to implement this change?

### Score

For each value (A, B, C, D, E) below, enter a number from the following sequence: 1, 2, 3, 5, 8, 13.

- **Impact:** A = 
- **Reach:** B =
- **Community/Goodwill:** C =
- **Confidence:** D = % [0 - 100]

**Score:** `=ROUND(((A*0.4)+(B*0.4)+(C*0.2))*((D/100)*10))`

## Effort

> How many 'person hours' of effort do you estimate required to make this change?

**Effort:** E = 
