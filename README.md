# Governance, Content, and Community Repo
This repository holds important documentation about our project, processes, content and community. 

> With the approval of [RFC 012 - Internal knowledge base](https://gitlab.com/tgdp/request-for-comment/-/blob/main/Accepted-RFCs/RFC-012-internal-knowledge-base.md), this collection of documents may be relocated in future.

## Project Docs
- [Project steering committee](https://gitlab.com/tgdp/governance/-/blob/main/ProjectSteeringCommittee.md)
- [Working group guidelines](https://gitlab.com/tgdp/governance/-/blob/main/Working_Group_Guidelines.md)
- [Passwords policy](https://gitlab.com/tgdp/governance/-/blob/main/passwords_policy.md)
- [Slack archives](https://gitlab.com/tgdp/governance/-/tree/main/Archives)
- [License](https://gitlab.com/tgdp/governance/-/blob/main/LICENSE)

## Content Strategy
- [Core strategy](https://gitlab.com/tgdp/governance/-/blob/main/Core_Strategy.md)
- [GitLab issue template](https://gitlab.com/tgdp/governance/-/blob/main/.gitlab/issue_templates/Default.md)

This is the main repo to raise issues for tracking content strategy work. The website and templates repo can also be used to track issues for specific content strategy tasks.

## Community
- [Code of conduct](https://gitlab.com/tgdp/governance/-/blob/main/CODE_OF_CONDUCT.md)
- [Community moderators](https://gitlab.com/tgdp/governance/-/blob/main/COMMUNITY_MODERATORS.md)



