[]{#anchor}Chronologue WG Notes

The following notes form the collective memory of this working group. Please make sure to note when and who said/did something. For more general information, see our [*repo wiki*](https://github.com/thegooddocsproject/chronologue/wiki/About-Chronologue).

**Miro board link**: [*https://miro.com/app/board/o9J_l1StfPA=/*](https://miro.com/app/board/o9J_l1StfPA=/)

Password: gooddocs\
**Prototype**: [*https://www.figma.com/proto/lvaAChlbueycET2ws9ZquS/Chronologue?node-id=902%3A1745&scaling=min-zoom&page-id=902%3A1640&starting-point-node-id=902%3A1745*](https://www.figma.com/proto/lvaAChlbueycET2ws9ZquS/Chronologue?node-id=902%3A1745&scaling=min-zoom&page-id=902%3A1640&starting-point-node-id=902%3A1745)\
**Templates dashboard:** [*https://gitlab.com/tgdp/templates/-/boards/4801048*](https://us02st1.zoom.us/web_client/9zdhk1t/html/externalLinkPage.html?ref=https://gitlab.com/tgdp/templates/-/boards/4801048)

**API specification doc**: [*https://docs.google.com/document/d/1iFB-6jpPiT52DbuhcZQ-BcEjmUqwYAuwYVHx6y-cN_o/edit?usp=sharing*](https://docs.google.com/document/d/1iFB-6jpPiT52DbuhcZQ-BcEjmUqwYAuwYVHx6y-cN_o/edit?usp=sharing)

**Documentation site:** [*https://docs-chronologue.netlify.app/*](https://docs-chronologue.netlify.app/)

[]{#anchor-1}2022-12-10 Wrap up meeting

Chronologue will be on holiday break until **January 14th**.

Tina will be on project break for a longer time. Unfortunately, she got too much going on at the moment and can't contribute in the capacity that she would like to.

However, [\@Valeria Hernández](https://thegooddocs.slack.com/team/U03H69W13C0) will run our Saturday meetings in the next year.

[]{#anchor-2}January 14th will be the kickoff meeting

Preliminary agenda:

-   [\@Peter Jurich](https://thegooddocs.slack.com/team/U03GGGLL31Q) and [\@Valeria Hernández](https://thegooddocs.slack.com/team/U03H69W13C0) will present their process and how sessions will work in the next year.
-   Release planning: Taking a look at the published templates and planning who's writing content for these.

[]{#anchor-3}What we did during the meeting

-   Merged in Peter's [request for recording](https://docs-chronologue.netlify.app/docs/t_requesting_a_new_recording/). ![](Pictures/100002010000002C0000002C87D703154E95048A.png){style="width:0.2362in;height:0.2362in"}

```{=html}
<!-- -->
```
-   We've also thought about our accomplishments for the year. We've made a lot of progress and it's worth calling out!

```{=html}
<!-- -->
```
-   **Mockups**: We've gone from a rough idea about the Chronologue in January from having a mock in April, thanks to [\@Serena Jolley](https://thegooddocs.slack.com/team/U02FZSBAHTM) and [\@Ulises de la luz](https://thegooddocs.slack.com/team/U02FD2KEQ0L) hard work.
-   **API:** [\@Ian](https://thegooddocs.slack.com/team/U01PM8FTKM5) did a great job with creating an API from the ground. I remember how we all filled a spreadsheet with fun astronomical events to have some API data.
-   **Docs tooling**: We've iterated a bit (built up and teared down 2 docs platforms) before landing on our final setup. Thanks to [\@Bryan Klein](https://thegooddocs.slack.com/team/U018BJR6JQN)'s support, we're getting closer to an actual docs website we can share.
-   **Group maturity**: We've had a lot of awesome conversations around how we want to work together, and had many new folks join this year. I've drafted an RFC (which I hopefully will finish today) that lies down the foundation for our group. [\@Valeria Hernández](https://thegooddocs.slack.com/team/U03H69W13C0) and [\@Peter Jurich](https://thegooddocs.slack.com/team/U03GGGLL31Q) have been continuing that work by thinking about processes and how we want to work with the template groups.

I thank everyone for their contributions. You rock!\
\_\_\_\_

[]{#anchor-4}2022-11-16 - Template to Chronologue process discovery

Attending:

Agenda:

-   Let\'s have an initial discovery meeting to talk about the process of handing off the templates to the Chronologue team. Key questions include:

    -   How does the Chronologue team track the status of templates that are currently in progress so that they can plan future work?

    -   When a template is complete, how does the Chronologue team know it\'s ready? (Does someone open an issue somewhere?)

    -   When the Chronologue team finds a problem with a template that requires a template rework, how do they re-engage with the templateers?

    -   How does the Chronologue team request a template that they need for a specific content type they want to create?

[]{#anchor-5}2022-11-12

Attending:

Notes:

-   Chat about organizational structure. Currently, both leaders feel stressed out, and people often don't have as much to do as they would like to contribute.\
    Let's talk about how we can distribute tasks!\
    Discussion starter (from [*RFC*](https://gitlab.com/tgdp/request-for-comment/-/blob/96ad7b3084c998501b43f8af43f99011e1e3d908/RFC-015-Chronologue.md#role-matrix))

![](Pictures/10000201000004A600000264A3A0FE89AB261B0E.png){style="width:7.5in;height:3.861in"}

-   Some functions we've identified:

    -   **Coordinator**: Keeps the rest of the Good Docs project up to date, and coordinates with the onboarding committee. \@Tina can continue doing this

    -   **Process warden:** Handover process between templates and Chronologue. Valeria likes to take this on. Next steps: Need to have a kickoff discussion with Alyssa.

    -   **Internal documentation**: Making sure that we have internal docs so we can maintain\
        > \@Sage would like to take over this.

    -   **Docs plan warden**: Keeps track of our documentation plan, if we are able to start new content for new templates

[]{#anchor-6}2022-10-29

Attending: Peter, Alyssa, Connie, Ulises, Ian, Sakura

Notes:

-   Alyssa gave an update on tech's team creation of two repositories so writers have a place to work

    -   More details in Slack discussion [*here*](https://thegooddocs.slack.com/archives/C016L3962CU/p1666395057678329).

-   Bryan gave an update on Hugo theme. He's welcoming ideas for how to change it, but says if it is a good enough starting point, we can start adding content

-   We decided to deprioritize creation of a custom theme so Connie can support tech team

-   Ian walked us through the current mockup on Figma

    -   Ian also fielded some general questions about tool, some we could not answer presently

    -   We discussed replacing the input box on the homepage with multi-select boxes to help guide users

        -   **ACTION ITEM: Ian will begin conversation with Ulises and Serena on purpose of input box**

    -   We discussed the video portion of the homepage and whether we should create an introductory or how-to video for the homepage. No action taken.

    -   Ian and Connie joined breakout room to review HTML

-   Sakura, Alyssa, & Peter stayed after to discuss flow of information between chronologue and templateers

    -   [*Current Templateer process flow here*](https://gitlab.com/tgdp/templates/-/blob/main/CONTRIBUTING.md)

    -   Alyssa explains that one Templateer focus now is in the creation of packs for users (starter pack; community pack for OSS, etc). Templateers are also focusing on building out a roadmap, which will make Chronologue work easier.

    -   Current plan: Chronologue should look at [*what's available*](https://gitlab.com/tgdp/templates), what's in progress (go to "[*Boards"*](https://gitlab.com/tgdp/templates/-/boards/4801048)), then develop a plan for what's needed and how to create samples for what's avail

        -   **ACTION ITEM: Peter will create a Google Doc matching completed templates to their corresponding Chronologue samples.**

            -   From here, we can determine our work loads and assign future docs.

    -   Scenarios worth planning for:

        -   What do we do if Chronologue needs an unavailable template?\
            What to do if there's an issue with a template?

            -   Can we just do a Merge request? Yes, but still need a communications piece

                -   Alyssa or Dee have merge rights for Templateers, but should we involve the orig. template author? Yes.
                -   Also depends on levels for changes: i.e. typos vs. reworking

[]{#anchor-7}2022-10-15

Attending: Peter, Ian, Alyssa

Notes:

-   Ian & API docs

    -   How to use [*Postman*](https://www.postman.com/)

    -   Session is recorded for others

-   Everything due by December

    -   Draft due dates?

        -   Peter:
        -   Sakura: beefing up [*glossary*](https://docs.google.com/document/d/1VpkuP3kCAG454WPwYf_rXbk5fBcnK0wAryKm3yB2kiU/edit#heading=h.s86cvjbz979z), but wrapping up; needs conversion to Markdown
        -   Asynchronous conversation about RFC

    -   Updates?

    -   Is what is group committed to still reasonable?

        -   Are documentations relevant to rest of release?

            -   Mock tool & set up docu sight

-   Action item

    -   Everyone read [*Chronologue RFC*](https://gitlab.com/tgdp/request-for-comment/-/merge_requests/32) by Oct 29

    -   At 29th: reprioritize tasks

[]{#anchor-8}

[]{#anchor-9}2022-09-24

Attending: Peter, Tina, Alyssa, Sakura, Valeria

Notes:

-   Ian: Mon, Tue, Wed after 6pm PT
-   Tony wants to do the HTML layout for Chronologue Mock tool
-   Ask Conny if she wants to contribute to the Chronologue Hugo theme
-   Since the Tech team can also work async, they will agree on a meeting time that works or distribute work asynchronously.
-   The writers need a regular meeting to check in to make sure the content still works together.
-   Peter and Valeria want to meet with Ian to work on Chronologue mock files.

Todo: [*peterjurich@gmail.com*](mailto:peterjurich@gmail.com) Set up a group meeting with Ian, Valeria, Peter, Sakura.

[]{#anchor-10}2022-09-17

Attending: Peter, Valeria, Tony, Alyssa, Connie

Notes:

-   Welcome any newcomers

    -   Connie: how can I help?

        -   Is more interested in doing tech stuff in chronologues, and writing w/templates

-   Any project announcements from the general meeting

    -   Prague updates

    -   Alyssa: Good Dogs project

    -   Alyssa: Brooklyn Bridge release finalized

        -   Top priorities per group are in GitLab

        -   Templates that will be completed: contributing guide, readme, API reference, release notes; will need to create examples for these in next release cycle

            -   Need to create open source community component for these

    -   Alyssa: RFC

    -   GitLab migration: will take about a month to be complete, but dependent on several variables

-   Discussion about changing meeting times and what could work well for people

    -   Valeria: Wednesdays are out, but new job coming up, so she won't know for sure by the end of the year

    -   Connie: Wednesday is out. Night-owl so time of day of current mtg is just fine for any other day. Can make it earlier on Mondays, Fridays.

    -   Deanna: open to moving times

    -   Alyssa: Mornings are very hard, but anything after 1:00 MT works.

    -   Tony: Tuesday evenings out; what about mandatory monthly on the same date, not day

    -   Cornelia: brought up splitting meetings.

    -   Tina: Can meet during weekdays except Tuesday

    -   Ian: Mon-Wed after 6 PST is good

    -   Ivar: ???

    -   Sakura: wide open, evening times are better during the week

-   Status updates from everyone on the team. Any blockers?

    -   Peter: request for recording is done, need to continue process; no blockers

    -   Valeria: continue working on comments, will then work on API overview, then reference; no blockers

    -   Sakura: finishing up glossary then wanting to move onto API

[]{#anchor-11}2022-09-10

Attending: Tina, Serena, Ian, Alyssa

Notes: \[Working meeting\]

[]{#anchor-12}2022-08-27

Attending: Tina, Alyssa, Valeria, Sakura

Notes:

-   **Release planning** (probably December)\
    **Writer's** tasks:

    -   Switch tooling from docusaurus to Hugo (Tina)

    -   Chronologue RFC (Lead by Tina, group effort)

    -   Release notes (TBD) (use the upcoming template, created by Rachel Steiner, Tara Sweeney)

    -   API overview (Valeria) (in progress)

    -   API reference (Valeria, Sakura) (yet to start, template upcoming by Mengmeng)

    -   Check in with Ricky (Tina)

    -   Glossary (Sakura)

    -   Request for time travel (Peter)

    -   Extended process flow diagram (Peter)

**Developer** tasks:

-   Ian and Tony:

-   Finalize the HTML structure and CSS styling for the first home page and the community page

-   Set up all website functions like form search, dynamic routing to a specific event page.

```{=html}
<!-- -->
```
-   RFC: Talk about our workflow

[]{#anchor-13}2022-08-20

Attending: Tina, Alyssa, Peter, Valeria, Ivarr,

Notes:

-   Walk around the room:\
    Ian: Tony and him had a meeting about the tech stack. They will build the HTML first, and style it so it looks like the mockup. They will add dynamic data fetching later on.

```{=html}
<!-- -->
```
-   Discussion of the Chronologue RFC: [*Chronologue RFC*](https://docs.google.com/document/d/1eVUFbevHJlQne1tF5gzGy_Z5FLtbg97pXQkadMLTmZc/edit?usp=sharing)
-   Sakura: Got assigned to the Glossary document. We agreed that Sakura can read up on our existing documents and collect terms that need to be explained.

[]{#anchor-14}2022-07-23

Attending: Peter, Alyssa, Sakura, Ulises, Ívarr, Tony

Notes:

High level introduction of TGDP and Chronologue project for Ívarr and Sakura.

Summary:

-   Chronologue is a mock tool designed by Ulises to be converted to a working website by Ian and Tony.
-   The docs side intends to demonstrate good documentation for the tool, along with commentary about why the documentation is effective.
-   The working website is intended as a tool that can support the documentation needs.
-   TGDP template team desires comments and feedback from writers about using their templates.

Discussion:

-   Sakura asked about enterprise vs open-source docs. Tony: Identical content needs based on audience and tasks, only difference is who does the work. Example of Microsoft correction via pull request. Alyssa: we're targeting the open source community due to experience of the group.

```{=html}
<!-- -->
```
-   Brooklyn Bridge period release period (Dec 2022) - key initiatives: Building out product man

    -   Template packs - suites of templates

        -   Basic starter documentation pack (based on [*Docs for Developers*](https://www.amazon.com/Docs-Developers-Engineers-Technical-Writing/dp/1484272161/) book)
        -   Community docs starter pack (based on GitHub's guide for a healthy community)

    -   Take design and build out various template packs that could be helpful

```{=html}
<!-- -->
```
-   Aside to Ulises and Ívarr to consider helping the Content Strategy working group to help make people find the Chronologue fake tool templates and examples, marketing the project with the goal of getting people to use the templates, and invite people to help the community.
-   Each of the D4D templates could be evaluated through the diataxis framework with a thought of

Announcement:

-   TGDP plans to avoid community burnout by taking first two weeks of August off from meetings. Overall, the project takes whatever we can give it. On return, doing more release planning.
-   Migration from GitHub to GitLab after other repos. Alyssa to advise new date.

Aside: Template group

-   API template is in review process now. Be good for Sakura to get involved with that to learn API doc needs and contribute feedback towards how the templates can enable her.

[]{#anchor-15}2022-07-16

Attending: Peter, Tina, Valeria, Alyssa, Ian, Serena, Mauricio

Notes:

Working meeting: Discussion of some comments in Peter's document: [*Request for new recording*](https://docs.google.com/document/d/1mbqRqScmEkM4yspH7vs6tL02aSDRFVpgmUfCbeQaxRE/edit?usp=sharing)

\
Should Chronologue be open for personal use?

Some personal events wouldn't fit with the astronomical data we are currently focusing on.

Let's focus on scientific and research purposes. Once we finish that phase, we can discuss expanding the project. Personal use would introduce a lot more use cases, needs user accounts and authentication → Can of worms.

Should you supply a date and time in the form?

Ian: Dates and times can be converted into ISO.

[]{#anchor-16}2022-07-09

Attending: Alyssa Rock, Tina Lüdtke, Bryan Klein, Valeria Hernandez, Ian Nguyen, Tony Chung, Sakura Ticer

Notes:\
\
**Discussion items**:

-   Should we move the meeting to a time during the week? The weekend might not be the best time slot for some people (Tina wasn't able to make it a few times, but it also could be the summer slump).\
    → Todo: Tina create *whenisgood* poll, check in with UXers on a regular basis.✅\
-   In the last general meeting, we discussed Chronologue in terms of the tools we are using. ([*Meeting notes)*](https://docs.google.com/document/d/13xr5PLMMqm6Rz_yXzxl5WMOwY9H6liB1abU_i0-ws60/edit#). We agreed that because we aren't using the standard tools (Hugo), there should be an RFC to line out why we differ from the usual tools and how we want to support this in the long term. Tina will create a draft and share it with the group.\
    Possible outcomes:\
    1. We get to keep the current tools we use\
    2. We switch to the tools the Good Docs project already uses and has tech support for, if all our **requirements** can be fulfilled.\
    → We need to discuss our wishes and technical requirements in an upcoming session\
    Todo: Tina creates a draft of the RFC so we can discuss details in the upcoming meetings.\

**Check in with the writers**:

-   Ricky: Not attending, checked in with Bryan to create a draft. Will follow up with Tina.
-   Peter: Not attending, made some progress on the recording process topic.
-   Valeria: Question about the audience: Is the audience developers/ tech writers?

\
**Check in with our tech team**:

-   Ian: Tech stack contains of Node.js, Next.js (React-based). Ian and Tony will meet to discuss the repo in more detail.
-   Tony: Had a great chat with Ian re: current state. Scheduled meeting Wed Jul 20 evening to walk through the tech stack in-person. Up 'til then, I'll lend my hand-coding experience to craft a workable HTML/CSS template using the Figma assets and Bootstrap as the basis.\
    \
    Ian to push a file to the repo that contains the base markup for his created components so far. Tony can start with that and update for W3C-compliance, then break apart back into components.

***Github to Gitlab migration***\
Chronologue will be the first repo to be migrated.\
\
**Presentation**: https://miro.com/app/board/uXjVOnVC4JI=/ Password: GitLabMigration\
\
**Recorded session**: https://youtu.be/\_O9Dde3DvJM\
\
**To Dos:**

-   Create a Gitlab account, preferably with the e-mail you are also using on GitHub. [*https://gitlab.com/users/sign_up*](https://gitlab.com/users/sign_up)
-   If you have an existing Gitlab account, link your Github account: [*https://gitlab.com/-/profile/account*](https://gitlab.com/-/profile/account)
-   Open a PR if you have a work in progress file on your local machine - even if you don't think it's ready to share yet. We will merge all PRs, so the move to Gitlab is easier.

Brian\'s demo of GitLab workflows: [*https://www.youtube.com/watch?v=btq4ev7cT_g*](https://www.youtube.com/watch?v=btq4ev7cT_g)

Alyssa\'s demo of GitLab project management features: [*https://www.youtube.com/watch?v=C_JWHezPjjg*](https://www.youtube.com/watch?v=C_JWHezPjjg)

For more questions, reach out to #tech-requests on slack.\

[]{#anchor-17}2022-06-04

Attending: Serena Jolley, Ulises de la Luz, Tina Luedtke,

Notes:

-   Short introduction round:

```{=html}
<!-- -->
```
-   Check-in with our UX designers Serena and Ulises about the prototype\
    Ian left some comments in the mockup that will be addressed later.\
    Question: Requesting process: Currently link goes to the community page since you can't make an actual request. However, we need documentation for the request process.\
    → Build fake form
-   Walkthrough around the Chronologue repo.\
    Question: Ian, do you need additional developers to build the website?

```{=html}
<!-- -->
```
-   Documentation:\
    Give an overview of our current docs set and assign topics to writers.

-   \[Optional, if time is left\] Documentation: Discuss evaluation form: [*https://docs.google.com/document/d/17aZPQ-J5lBzSw8FUTOAdeNnxvGe12Or2r55Jh1i6OMI/edit?usp=sharing*](https://docs.google.com/document/d/17aZPQ-J5lBzSw8FUTOAdeNnxvGe12Or2r55Jh1i6OMI/edit?usp=sharing)

-   Two items from Alyssa:

    -   Can someone possibly represent this group in the Release Planning meetings while Tina is out? It would require attending the general US meeting on June 22 and June 29 from 3-4 PDT, possibly also July 6. I need someone who has been with the Chronologue long enough to have a context for the work being done here.\
        > → Tina can make it to 29/06 and 07/06 but can someone sit in 06/22?\
        > → Serena can attend the brainstorming session on 22th.

    -   Circling back about the Contributing Guide template feedback. → Connect with Ian: Probably low priority since we don't heavily advertise that people can contribute.

-   Release plan: What do we want to achieve until Oct?

```{=html}
<!-- -->
```
-   Your agenda item!

[]{#anchor-18}2022-05-28

Skipped in favor of memorial day weekend.

[]{#anchor-19}2022-05-21

Skipped since most of us meet each other at writing day (05/22)

[]{#anchor-20}2022-05-14

Attending: Serena Jolley, Ulises de la Luz, Tina Luedtke

Notes:

-   Serena got approval for a ticket at WtD. She will get some feedback from table participants on Writing day. → Does Hopin allow creating new rooms?
-   Introduced Ricky to the project and sent him links to the Chronologue and website repo.
-   Tina resumed working on the Chronologue Slides for Portland.

[]{#anchor-21}2022-05-07

Attending: Serena Jolley, Ulises de la Luz, Tina Luedtke

Notes:

\- **Mock tool**: [*\@Ulises de la luz*](https://thegooddocs.slack.com/team/U02FD2KEQ0L) Can you give us an update on the tool? I know you've asked for a deadline a while back and I said end of April, so I wanted to see where we're at, and how we can help you in case you need more feedback.

→ Getting more feedback today\
→ Serena and Ulises still need tickets for write the docs

→ We might need a sign up sheet to conduct structured 1:1 usability testing. Serena thinks more about the logistics: Will we conduct test during the conference or after? (Depends on the ticket situation).\
\
Current version of the mocks: [*https://www.figma.com/proto/iXEPz9xxucwHpY6HfXWSCH/Untitled?node-id=1920%3A3984&scaling=min-zoom&page-id=1692%3A2267&starting-point-node-id=1920%3A3592&show-proto-sidebar=1*](https://www.figma.com/proto/iXEPz9xxucwHpY6HfXWSCH/Untitled?node-id=1920%3A3984&scaling=min-zoom&page-id=1692%3A2267&starting-point-node-id=1920%3A3592&show-proto-sidebar=1)

-   **API**: Task for the WtD writing day: People can contribute by creating a new PR.\
    Ian still needs a ticket and he can support people in this task.\
    API will probably change a bit to work with Netlify.

Rest of the meeting: Working on the high fidelity mock up, and the Chronologue presentation.

ToDos:\
Tina: Presentation for WtD\
Tina: creates a draft for the API documentation (reference, concept, quickstart); let's finish the API within the next 2 months. After that we can build the mock tool.

Serena: Figures out the ticket situation and if/ in what capacity she wants to run a user test

Ian: Get API to function with the Netlify functions feature.

[]{#anchor-22}2022-04-30

Attending: Serena Jolley, Ulises de la Luz, Alyssa Rock

Notes:

-   Serena and Ulises mostly spent this hour coworking on the mockup design.

[]{#anchor-23}2022-04-16

Attending: Tina Lüdtke, Serena Jolley, Ulises de la Luz, Bryan Klein, Alyssa Rock

Notes:

-   The chronologue docs will be on the chronologue website ([*thegooddocsproject/website-hugo*](https://github.com/thegooddocsproject/website-hugo))
-   Application will be in the chronologue repo: We could host on Netlify (OSS subscription): Function could serve the API.\
    We could look into vercel for hosting as well\
-   We can build everything on GitHub, then do a code freeze and migrate to Gitlab.\
    → Reference: Look at how the Glossaries project is set up. In the source: Docs/Glossaries; We could have a subfolder Docs/Chronologue.\
    Swagger is already built into the website. If we stick to the OpenAPI specification, it will convert into docs automatically. API can live in the chronologue repo.\
-   UX: For the first version, let's stick to a simple approach: Reference things in the text and add them in a related links section. Initial idea was to have highlights when you hover over a link; too complicated for now.
-   Write the Docs Portland: Who's gonna attend?\
    Serena (requests at work); Ian (looks into it); Tina (has ticket but has to confirm if she can keep it); Alyssa (yes);
-   How do we want to represent Chronologue for WtD?\
    We probably need a few more technical writers to test the templates. We could also benefit from some more technical folks to build our page.\
    \
    Todo: Tina reads up on Swagger and Open API so we can check how to restructure our current API data into the Open API spec.\
    Check Swagger vs Postman
-   Todo:\
    → Get clear on requirements and then create a pitch for the Chronologue projects.\
    → Prepare some simple tasks that someone without all the context could do\
    Simple task: Contributing to the data of the API: Creating new entries. Limitations: Location has to be real.\
    Serena and Ulises could do a user test during the conference.
-   Serena and Alyssa won't join next week

If we want to attract new writers to Chronologue/ Pitch ideas for chronologue\
- We should cater to people that want to break into tech writing: Testing templates is a good way to start and build portfolio.\
- People that want a writer community and are already writers: We give you actual tasks instead of just talking about docs.

[]{#anchor-24}\
2022-04-09

Attending: Tina Lüdtke, Ian Nguyen, Ulises de la Luz

Notes:

-   Tina wants to have a look at the mock and identify components that we need for the mock tool\
    These ideas are captured in the Chronologue [*Design doc*](https://docs.google.com/document/d/1SY0e2dG0rOO3RqR7ITulsvx15cqVBKUcjgod_nFGpjo/edit?usp=sharing)

![](Pictures/100000000000064C000005582EE8B3BBBE6BA0AD.png){style="width:7.5in;height:6.361in"}

[]{#anchor-25}2022-03-26

Attending: Tina Lüdtke, Alyssa Rock, Ian Nguyen, Ulises de la Luz, Serena Jolley

Notes:

-   Serena: Finished usability testing and will incorporate these into high fidelity mockups.
-   Ian: [*Finished the AP*](https://github.com/thegooddocsproject/chronologue/tree/main/API)I; Lacks documentation → He will use a template and provide feedback
-   Tina: Started a docs plan: [*https://github.com/thegooddocsproject/chronologue/edit/main/documentation_plan.md*](https://github.com/thegooddocsproject/chronologue/edit/main/documentation_plan.md)
-   Reach out to templateers to ask them which templates should be tested.
-   Start to draft documentation
-   Once we have a set of docs, we can start implementing (probably Hugo/ Docsy)
-   When it comes to building the mock tool: We might need to reach out to more volunteers.\
    We will probably build the tool using React.

[]{#anchor-26}2022-03-19

Attending: Tina Lüdtke, Alyssa Rock, Ian Nguyen, Ulises de la Luz

Notes:

-   Ulises: Status update on the UX tests:\
    Test the wireframes, will be done probably next week. Will improve the design.\
    It would be good to have a high fidelity prototype until May.

-   Ian: Pushed a planning doc to our repository: [*roadmap.md*](https://github.com/thegooddocsproject/chronologue/blob/main/roadmap.md)\
    Proposes: Next.js as a framework to generate our website and host our API.

-   Documentation requirements:\
    Users: We want to cater to people who are not technical writers. Target audience is Developers.\
    Reuse yes/no: Most likely, Developers don't need to learn about reusability.\
    Ian: Quickstart guide, document API routes, Bug reporting guide;\
    Alyssa: These things are in the works or already done.

-   Alyssa: Purpose of Chronologue in terms of consumers:\
    Primary: Exhibit good documentation practises and provide an example\
    Secondary: Act as a QA for the other arms of the good docs project

-   Tina: Reads Docs for developers: Can come up with a documentation plan. Once we know what pages we need, we can work on information architecture.

-   Good examples: [*https://docs.github.com/en*](https://docs.github.com/en); [*https://www.docsy.dev/docs/examples/*](https://www.docsy.dev/docs/examples/)

-   Requirements for our documentation website:

    -   Table of contents - whole site

    -   Table of contents - page specific

    -   Content page: Optimized for readability: up to 80 characters per line, enough whitespace, formatting options like bold, italic, code

    -   Last modified feature:

![](Pictures/1000020100000331000000219927E3CE59BCA22E.png){style="width:7.5in;height:0.3055in"}

\* Implementing feedback similar to github if possible:

![](Pictures/10000201000001340000015F7329B11B16A06168.png){style="width:3.2083in;height:3.6563in"}

\* "Make a change" workflow: Click on a call to action on the docs page and get redirected to the source; make changes; open a pull request.\
\* Accessibility: We should try to be as accessible as possible from the get-go. Let's see if we find a tool that checks accessibility. Helpful resources: [*https://www.w3.org/WAI/ER/tools/*](https://www.w3.org/WAI/ER/tools/) [*https://www.npmjs.com/package/a11y-sitechecker*](https://www.npmjs.com/package/a11y-sitechecker)

[]{#anchor-27}2022-03-12

Attending: Tina Lüdtke, Serena Jolley, Ulises de la Luz, Alyssa Rock

Notes:

-   Doctools group: Less about advocating for one tool, but will create a specification for a good doctool system. Will create case studies to showcase how these requirements are implemented.\
    → We are a customer of the doctools group.\
    → The doctools team needs to find the heartbeat and cadence; They work on the Gitlab migration first, after Portland they will do the core work of their group.\
    → Discuss requirements with them. (Maybe in 3 weeks from now)?
-   Idea: Make documentation content independent of the platform;
-   If we have requirement for what a documentation tool should support, we can talk to them
-   "As a documentation system owner, I want to set up a new tool and I want to be guided through that process like a wizard".
-   Documentation has to be in Markdown because all of the templates are in markdown. At some point we could expand the templates for other languages.
-   Template update: They get the most new people and they figure out a system for onboarding & training new templateers.
-   Mengmeng creates a group specifically dedicated to API docs templates. Some of them already exist and
-   If we want more people to join Chronologue, we probably should improve our messaging
-   Serena/ Ulises update: Card sorting is finalized. They now have created a low fidelity mockup to test user flows.

[]{#anchor-28}2022-02-26

Attending: Tina Lüdtke, Serena Jolley, Ian Nguyen, Ulises de la Luz

[]{#anchor-29}Notes:\
Infrastructure and Tooling discussion:\
Serena: Doesn't need additional tools. Finished card sorting exercises. Analyse the results of the card sorting. After that they can plan the usability test.

Our code base is in Javascript: We could insert comments and extract that as documentation, for example with ​​ [*https://jsdoc.app/*](https://jsdoc.app/).

Requirements:\
- We need easy to build multi column content to annotate our docs.\
- Depending on the audience/ use cases:

-   We can just document the codebase. (jsdoc)

```{=html}
<!-- -->
```
-   Non-coders can read static sites (Antora)

Possible Static site generators:

-   [*https://antora.org/*](https://antora.org/) → Works well with JS and AsciiDoc. It sounds like a good fit for our purposes.
-   [*https://www.sphinx-doc.org/en/master/*](https://www.sphinx-doc.org/en/master/)
-   [*https://docs.readthedocs.io/en/stable/tutorial/*](https://docs.readthedocs.io/en/stable/tutorial/)
-   [*https://fosscord.com*](https://fosscord.com)
-   [*https://squidfunk.github.io/mkdocs-material/*](https://squidfunk.github.io/mkdocs-material/)

\
To Do: Ping doctool group that we want to set up that tool. Ask them where to set up the tool (new repo vs Chronologue repo). Should we document our journey of setting up this tool? ---\> Done by Tina

Open question:\
Right now, we only illustrate the "publishing" phase of documentation: "What do good docs look like".\
We could also explain something more about successful documentation creation processes and pipelines, for example in a blog post.

[]{#anchor-30}\
2022-02-12

Attending: Tina Lüdtke, Serena Jolley, Ian Nguyen, Alyssa Rock

Notes:\
Discussion about [*Chronologue documentation*](https://github.com/thegooddocsproject/chronologue/wiki/Chronologue-documentation):

-   When we request a new template we should give them actionable requirements and priorities
-   QA function: It makes sense to let the templateers do their work and we have the fresh set of eyes. → Not interfering with the template creation.
-   We need to work on doctools (soon); We also have to set up some requirements. Here, the interaction can be closer.\
    Update from the doctools group: Concept: Making it easy to clone a doctool repo and get it up and running. They create a specification of what you need to get up and running a certain doc tool.

Update from Serena: They are running the card sorting sessions and will begin with actual user tests soon. Once they collected all the feedback, they will create a high fidelity mockup in Figma which allows us to copy at least the styling of the elements.

Update from Ian: He finished the API and is thinking about tooling for the website.

\
To do in next session: Discuss about our docs requirements on the 26th of March

[]{#anchor-31}2022-02-5

Attending: Tina Lüdtke, Serena Jolley, Ian Nguyen, Alyssa Rock, Ulises de la Luz

Notes:

-   Metacommentary - How we decide to write docs, what is effective in the docs. We need to make sure that our docs setup supports \<aside\> or two column layout.
-   Soft requirement: (Fictional) Open source community; Another group within the good docs creates best practices for community governance. → We can have a "community". Fictional users that request time with the Chronologue can make observations & study. Whatever information they find, they can add it to the Chronologue website.
-   Ian: Server for the API and website can be one place. We build the site with react. Waiting for **UX**: Prototype; Funding: Currently no money needed
-   Serena: How far should we go with the user experience? What happens when someone fills out a submit form?
-   Tina: Continues with Wiki

[]{#anchor-32}2022-01-15

First formal meeting of the year 🎉\
Attending: Tina Lüdtke, Serena Jolley, Ian Nguyen, Alyssa Rock, Ulises de la Luz\

Notes:

What do we achieve until Write the Docs Portland? (22nd May -24th May)

-   Have high-fidelity prototype for the website ready
-   Have a usable API (Ready to make calls to it)
-   Identify what documents we need to provide for the documentation (rudimentary IA); check what templates could help us
-   Research potential backend and doctools

\
How do we want to use the writing day?

-\> Documentation for the API\
-\> UX: User research and usability testing

-\> Potentially set up a basic set of pages on the website\
\
\
How do we want to work with the templateers?\
→ In the main meeting, we thought of pair writing sessions.

Update from our UX group:

Ulises and Serena will think of a usability test to help smoothen out the mockups. Maybe 30 mins, possibility to contact the same people in a later stage\
\
Update from API design:\
Once the API is finished, we should test it.\
Re: If people want to contribute: They should open a PR if they want to make changes to the main repo.\
Create repositories in the thegooddocsproject Github.\
- chronologue-api\
- chronologue-docs\
\
Hosting the API: We need to find a place to host the API. Potentially we can host at Google or Microsoft for free if the traffic is low.\
\
Alyssa:\
What are our long term plans?\
-\> Have a functional mock-product\
-\> Create a docs site: Get in touch with the doctools-easy-button project after the conference. Based on the feedback and mockups we can get recommendations for our docs pipeline.\
\
Questions for later planning sessions:

Is there a need for an unconference session?

Who will go to WtD Portland?

[]{#anchor-33}\
2021-11-16

Attending: Tina Lüdtke, Serena Jolley, Ian Nguyen, Ulises de la Luz

Notes:

Working on the Chronologue API:

-   Populating the API with Data

[]{#anchor-34}2021-11-16

Attending: Alyssa Rock, Serena Jolley, Ian Nguyen

Notes:

-   Communication between the template team and the Chronologue team

    -   We need to figure out how the handoff from the templateers to the Chronologuers will work.

    -   Two possible models:

        -   Model 1 - The templateer works on creating a Chronologue example. If we use this model, the Chronologuers would need to create onboarding materials to help templateers get up to speed on the tool.
        -   Model 2 - The Chronologuers actually take a template and see if they can effectively implement the template in the Chonologue project as-is. In this model, the Chronologuers would essentially act as QA for the template and provide feedback about whether the template was usable or not so that the template could then make further revisions.

    -   Currently, our team favors model 2.

-   Coworking sessions with the UX team led by Serena will be open to everyone, every Saturday weekly. Invites will be sent out.

-   Timeline for deliverables: End of March 2022 is the target deadline for the first Good Docs Project release. Members in each working group will set their own working group goals and milestones for their current tasks.

-   API:

    -   License discussion: using Zero-clause BSD right now; Alyssa will check to see if there are any issues with using MPL 2.0 in the API

    -   API documentation can be used as a good reference for the API templates

    -   The api's theme will focus on light-hearted themes

    -   Authentication and rate limiting will be a future feature after official launch

    -   API is not open to contribution when it is not launched yet

[]{#anchor-35}2021-11-02

Attending: Alyssa Rock, Tina Lüdtke, Serena Jolley, Ian

Notes:

-   Tina has created her Chronologue concepts document and would love to get some feedback on her initial document: [*Chronologue concepts*](https://docs.google.com/document/d/1GXEKk53e42CJ9qoXqX-tvSwSRoylBc_rMXH1ZbH9p2I/edit). So far, Cameron has given some useful feedback but she would like some more feedback from the rest of the group, if possible.

-   Serena and Ulisses met last week to better define user personas, user journeys, and user stories for the Chronologue project. They'll meet again next week during our off-week to further refine these documents and they'll likely be ready to share them with the rest of the group next time.

-   We spent some time making sure that Tina's document and Serena and Ulisses' work was aligned. We ironed out some of the small discrepancies between their work.

-   Ian and Tina will meet to begin working on the next steps for the API design. They'll start by working on defining the requirements for the API. They'll sync offline and decide on a time to meet next week when the group is out.

-   Tina is concerned that the Thigpen backstory will be too difficult to document and she'll post a message on Slack expressing her concerns for discussion with the rest of the group.

-   We discussed [*Felicity's Slack post*](https://thegooddocs.slack.com/archives/C016L3962CU/p1635882772009000) about creating an example project using the Corg.ly service in the [*Docs for Developers*](https://docsfordevelopers.com/) book. The group expressed some concerns about whether this fictional project would meet the Good Docs Project requirements because it doesn't have as much depth as the Chronologue project. Tina was also concerned about whether the science behind the Corg.ly service would be easy enough to flesh out (like she has done with the Chronologue project so far). We've also made some good traction on the Chronologue project so far and feel like we'd like to continue with that project for now.

    -   However, it's worth noting that Tina and Alyssa have organized a [*Write the Docs meetup*](https://www.meetup.com/virtual-write-the-docs-west-coast-quorum/events/281680933/) with the authors of Docs as Developers where they will share their reasons for writing the book and some of the key insights they've gained from the project.

    -   Dee from the Good Docs Project community is going to kick off a book club for this book soon if you would like to participate.

-   Things going on in the rest of the project that could impact this group:

    -   We're going to try to complete a few RFCs around the release management process. If those RFCs are accepted, the PSC might ask this group (and all working groups) to spend some time in December defining what their goals for this release will be. The release would likely occur in March or April.

    -   The Community Docs working group has two new templates that are in the community review phase: Deanna's tutorial templates and Carrie's bug issue template.

[]{#anchor-36}2021-10-19

Attending: Alyssa Rock, Tina Lüdtke, Ulisses de la Luz, Serena Jolley

Notes:

-   Alyssa and Tina are still working on their project deliverables. Tina is working on a conceptual document about time travel mechanics and Alyssa is working on a project README for the Chronologue group.
-   Serena and Ulisses have arranged a separate working session for them to develop the user personas for the Chronologue project.
-   We discussed whether it would be possible to do user research for this project. Alyssa mentioned we might be able to have Chonologue project members role-play as potential users.
-   Tina has mentioned she thinks it would be best if the Chronologue can be grounded in the science of what is theoretically possible. She thinks it will be easier to get new project contributors up to speed that way since it won't require them to understand science fiction.
-   For simplicity in user persona design and to support the Good Docs Project documentation project, we made a decision that we're going to make the Chronologue focused on time tourism and that it won't enable actual time travel, but rather simply viewing the past through a screen. We can always expand the tool's capabilities as needed later.

[]{#anchor-37}2021-10-05

Attending: Alyssa Rock, Tina Lüdtke

Notes:

-   We welcomed Tina, a new technical writer based in the Bay Area. Welcome!

-   Tina and I spent some time getting her up to speed on the Chronologue world, the project overview, and the contributing team members.

-   We spent a good chunk of time brainstorming possible user segments, personas, and use cases for the Chronologue tool. All our brainstorms are captured on the Miro board.

-   Action items before next time:

    -   Tina will create an example of a conceptual document that explains how time travel works to help us flesh out the logistics of time travel a little more.\
        > [*https://docs.google.com/document/d/1GXEKk53e42CJ9qoXqX-tvSwSRoylBc_rMXH1ZbH9p2I/edit?usp=sharing*](https://docs.google.com/document/d/1GXEKk53e42CJ9qoXqX-tvSwSRoylBc_rMXH1ZbH9p2I/edit?usp=sharing)

    -   Alyssa will create an example of a README doc using Sachin's template for the Chronologue project and tool.

    -   Alyssa will also talk to Bryan or Morgan about next steps for setting up a Chronologue doc site, but we can create the docs in the Chronologue repo for now.

[]{#anchor-38}2021-09-21

Attending: Alyssa Rock, Erin McKean, Ulises de la Luz

Notes:

-   We welcomed Ulises, a new UX designer. He's going to work with Serena to design the UI mockup for the Chronologue. Yay!

-   We used the Miro board to brainstorm tasks for the next steps with the Chronologue project. We'll get started on these tasks in our next meeting, which will be a working meeting.

    -   Erin hopes to work on developing the decision log for any decisions we make about the world and the tool. We might also start working on defining our publishing processes and policies.

-   Action item: Alyssa will post a poll on the templates channel (and cross-list it from a few channels) asking people for nominations about which template we should create an example for first.

-   Action item: Alyssa will also connect with Ulises and Serena this week to talk about first steps for building a UI mockup. Will we need personas and user stories first? Let's talk!

-   Links we shared:

    -   [*https://uxwriterscollective.com/category/the-dash-newsletter/*](https://uxwriterscollective.com/category/the-dash-newsletter/)

    -   [*https://web.hypothes.is/*](https://web.hypothes.is/) - Could be a great tool to provide public annotations about what makes our documentation site effective. Let's explore it more!

[]{#anchor-39}2021-09-07

Attending: Alyssa Rock, Morgan Craft, Serena Jolley, Erin McKean

**Agenda / Notes:**

-   Ran through the Miro board

```{=html}
<!-- -->
```
-   Talk about our intended audience \-- whether they are Tech Writers, and what experience level of Tech Writers. Or developers. So understanding that most of our readers will not be existing senior tech writers.

```{=html}
<!-- -->
```
-   Discussing whether we provide Bad Docs type of styles and show the corresponding good docs version. Discuss from Erin McKean tied to [*Confusing Explanations*](https://jvns.ca/blog/confusing-explanations/) around framing them the concept around how we write better docs.

    -   Avoid [*Fumblerules*](https://en.wikipedia.org/wiki/Fumblerules)

-   Discussion around whether or not we wait for Base Templates, or backup into the Draft Templates.

-   Morgan brought up the current clock synchronization [*Network Time Protocol: NTP*](https://en.wikipedia.org/wiki/Network_Time_Protocol)

-   *MesoAmerican Culture and Calendrical Conversion*

```{=html}
<!-- -->
```
-   [*Here's the original Chronologue README*](https://github.com/thegooddocsproject/chronologue/blob/main/README2.md)

```{=html}
<!-- -->
```
-   \"Fondation Chronomateur (FC), a Paris-based organization with a charter for the regulation of time travel across jurisdictions, exists as early as 2241, in only 14% of viable timestreams does the CLF take over management of the Chronologue standard and associated tools. \"

    -   This organization maintains the standard for the time-travel (thigpin) nodes via a hard-to-construct [*mcguffin.*](https://en.wikipedia.org/wiki/MacGuffin)

    -   Erin was thinking that this would be full on 1950's time-travel idea, you could go back and visit/change time if you wanted.

    -   Connie Willis \-- dooms day books

    -   Also the idea of being able to ask a device for the temperature at some location back in time.

    -   Device is more complicated than the microwave and less complicated that the Tardis (doctor-who).

    -   Depending on where you are going in the past, aka, going to the Maya time-line, and using the Thigpen Temporal Access Node as a viewer

-   Some links that were shared (sorry for the link dump from chat!):

    -   [*https://jvns.ca/blog/confusing-explanations/*](https://jvns.ca/blog/confusing-explanations/)

    -   [*https://en.wikipedia.org/wiki/Fumblerules*](https://en.wikipedia.org/wiki/Fumblerules)

    -   [*https://www.oxfordreference.com/view/10.1093/acref/9780195108156.001.0001/acref-9780195108156-e-72*](https://www.oxfordreference.com/view/10.1093/acref/9780195108156.001.0001/acref-9780195108156-e-72)

    -   [*https://github.com/thegooddocsproject/chronologue/blob/main/README2.md*](https://github.com/thegooddocsproject/chronologue/blob/main/README2.md)

        -   \"Fondation Chronomateur (FC), a Paris-based organization with a charter for the regulation of time travel across jurisdictions, exists as early as 2241, in only 14% of viable timestreams does the CLF take over management of the Chronologue standard and associated tools. \"

    -   [*https://github.com/thegooddocsproject/chronologue/wiki/About-the-Chronologue-universe*](https://github.com/thegooddocsproject/chronologue/wiki/About-the-Chronologue-universe)

    -   [*https://en.wikipedia.org/wiki/Connie_Willis#Novels*](https://en.wikipedia.org/wiki/Connie_Willis#Novels)

[]{#anchor-40}2021-08-21

Attending: Alyssa Rock, Morgan Craft, Serena Jolley

Agenda/notes:

-   Welcome!

    -   Serena (one of Alyssa's coworkers) joined us today. Always nice to see new people!

-   A note from Alyssa

    -   Now that the template contributing process has been submitted to the community for comment, Alyssa is more free than she had previously been. She plans to make the Chronologue project one of her key focus areas.

-   Meeting times and meeting cadences

    -   Alyssa's thoughts: I think we should ask ourselves who are the core members of the Chronologue team and design the schedule around the preferences of people who consistently show up.

    -   We're going to tentatively meet on a 2-week cadence, with our next meeting on September 7 at 4p.m. Eastern/2 p.m. Mountain.

-   Morgan created a [*JobsToBeDone document*](https://docs.google.com/document/d/1jcuvf1olMbC1-lGKlsFDVWZ3U3nX5IhO1A7CcxdyBC8/edit?usp=sharing) for the Chronologue project.

    -   This document helped us to talk more about the project requirements: how it needs a UI element, how it needs an API, etc.

    -   See some additional notes on Morgan's document.

-   Alyssa created a Miro board to help us brainstorm our vision for the Chronologue project.

    -   Miro board link: [*https://miro.com/app/board/o9J_l1StfPA=/*](https://miro.com/app/board/o9J_l1StfPA=/)

    -   Password: gooddocs

    -   Let's encourage everyone to use this Miro board to brainstorm the requirements and tasks that we need in order to bring the Chronologue project to fruition.

[]{#anchor-41}2021-07-17

Attending: Alyssa Rock, Andreas Stein

Notes:

-   Today it was just Alyssa and Andreas so we brainstormed some next steps for the Chronologue and talked about what we need to get this project up and running.

-   We also turned it into an impromptu Git learning session, which was a lot of fun! We talked about:

    -   What it is like to work in the CLI (command line) vs. the UI on Windows machine

    -   Recommended tools for using Git on Windows:

        -   Windows Subsystem for Linux (WSL)
        -   Ubuntu
        -   Windows Terminal
        -   Chocolately app

    -   And we ran through some helpful resources to start learning about Git:

        -   [*Git for the True Beginner*](https://www.youtube.com/watch?v=zJw6KNvmuq4) (1 hr presentation about the basic concepts of Git by Alyssa)
        -   [*How to set up the Windows tools*](https://gitlab.com/saltstack/open/docs/salt-install-guide/-/blob/master/CONTRIBUTING.rst) (Contributing Guide for Salt Docs)
        -   [*Learn Git Branching*](https://learngitbranching.js.org/) (a fun, interactive tool that visualizes what happens in the CLI)
        -   [*GitHub: No CLI required*](https://www.youtube.com/watch?v=CWfiFUNyxHc&t=1s) (a presentation about the GitHub UI from WTD Bay Area)

-   Next meeting: Saturday, August 21 at 3pm PST/Sunday, August 22 at 8am Australia Eastern Standard

[]{#anchor-42}2021-06-27

Attending: Ryan Macklin, Alyssa Rock, Andreas Stein, Anu R, Felicity Brand, Erin McKean

Notes:

-   About the Chronologue project

    -   This project started as Erin's brainchild in Google and will now be used as a way of generating example documentation for a fake tool (the Chronologue) for the Good Docs Project

    -   When we're talking about things that are happening in the world, we sometimes use the phrase "on stage" when we're in the world of the Chronologue and "off stage" when we're not inside the world of the Chronologue

-   Today's activity was to begin creating characters that will populate the world of the Chronologue using lessons learned from Ryan's background (and Andreas' background!) in tabletop RPGs.

    -   Ryan explained how to use his [*Chronologue character creation sheet*](https://docs.google.com/spreadsheets/d/1Qm1UQy9dznP2zI1EsqXJVZLmjOfqPvpyPj7hbalUOfE/edit?usp=sharing)

    -   We each created a contributor character using our personal tab, which contains a copy of the Chronologue character template

-   Action item: introduce your character on the #chronologue-docs channel

    -   These characters have met at the Timehoppers conference and have joined the Chronologue project!

    -   Now each character will introduce themselves on our #chronologue-docs channel, approximately one intro per day starting with Anu's contributor character

    -   When you introduce your character, think about what 3 things each character cares about the most that they want to mention. Example: Iliana has an [*Instagram for her cat*](https://www.instagram.com/blessedorb/)!

    -   If you want to create an avatar for your character: [*https://avatarmaker.net/*](https://avatarmaker.net/)

-   Action item: In the week after introducing our contributor character, each person can then introduce their Chronologue user character

-   Action item: figuring out how these characters will work on the Chronologue project

    -   After we've introduced the contributor characters, it's time to figure out what role they will have on the project.

    -   We could possibly use the All Contributors method to create a page for these characters and use emojis to demonstrate how they contribute to the project

        -   See this example in the [*All Contributors project*](https://github.com/all-contributors/all-contributors)
        -   See the [*contributor emoji key*](https://allcontributors.org/docs/en/emoji-key)

    -   Erin will see if the GitHub Discussion add-on could be a good way for these characters to converse with each other

-   Helpful links

    -   [*Ryan's talk about empathy advocacy*](https://www.youtube.com/watch?v=1yXdXYFTM_U)

    -   [*The Chronologue wiki*](https://github.com/thegooddocsproject/chronologue/wiki)

-   Next meeting: Saturday, July 17 at 3p.m. PDT/Sunday, July 18 at 8a.m. Australia time

    -   We'll shoot for a monthly meeting cadence

[]{#anchor-43}2021-04-22

Our [*working group page is live*](https://thegooddocsproject.dev/working-group/chronologue/)!

[]{#anchor-44}2021-03-21

Attendees: Alyssa, Erin, Felicity

-   We talked about the reasons why having an example project is good and what we want to be able to do with it. See About Chronologue

-   We talked about the idea of Chronologue and whether it will fit our needs. We decided that it will. See About Chronologue

-   Logistics:

    -   We\'re going to keep our core working group small to start with.

    -   We\'ll create something that shows the shape and intention, then we can open it up to invite contributions from others.

    -   To keep overhead light, we\'ll work in the wiki of the Chronologue repo until we outgrow it.

    -   We\'ll communicate via Slack. We\'ll work asynchronously. We\'ll meet monthly and can meet ad-hoc if the need arises.

    -   We imagine our deliverables will emerge like any typical OS project. We\'ll probably start with the ReadMe and then whatever makes sense next like an issue template, contributor guidelines, whatever.

    -   We decided we won\'t necessarily start with finalized Good Docs Project templates. We\'ll use what we need to make sense for the project and clearly label any template is still in draft. Any work we do on templates for Chronologue can be used to seed/feed templates in The Good Docs Project.

-   Double docs:

    -   Because this is a fake open source project, all docs will be doubled. We\'re going to need a ReadMe about Chronologue, and a ReadMe about contributing to the Chronologue project.

    -   We decided the docs are parallel, and internally consistent. We\'re using the paradigm of onstage (for Chronologue) and backstage (for everything to do with the planning and working with the Chronologue project). We might have parallel repos.

    -   We\'ll make sure any links in an onstage doc that need to link \'out of world\' go to the parallel backstage doc.

    -   For us world-builders, and any contributors, we\'ll need to have a very clear boundary of where on-stage ends, and backstage begins. Perhaps this will be obvious? We\'ll find out in time.

-   We will want a web component.

    -   Erin owns chronologue.dev. Let\'s also get chronolog.dev and setup a redirect.

    -   We could have a Docsy site inside a folder inside the existing Chronologue repo.

    -   We\'ll need a way to signal that this is fake. Something obvious in the footer and any links should pop-up a \'this is fake\' message or go to ReadMe which explains the fakeness of Chronologue.

    -   Building a fake project and fake community docs could be risky. We don\'t want a War of the Worlds situation happening. We\'ll make sure any links to emails or pages show an auto message \"example purposes only - check out our real README over here\". So that means as part of our MVP, we\'ll need an \'About the Chronologue\' page so that all links can point to it.

    -   Add a custom footer
