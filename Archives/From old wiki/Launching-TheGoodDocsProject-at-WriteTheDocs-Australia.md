# Launching TheGoodDocsProject at WriteTheDocs Australia, and conference highlights

(Archived from old wiki. Last edited by Cameron Shorter, "3 years ago")

* Date: 2019-11-25
* Blog post: http://cameronshorter.blogspot.com/2019/11/launching-thegooddocsproject.html
* Tweet: https://twitter.com/cameronshorter/status/1198893725239263233

Sent details to: 
* WriteTheDocsAu Attendees 2019 <attendees2019@wtdau.topicbox.com>, 
* thegooddocsproject@groups.io, 
* newsletter@writethedocs.org
* Slack:WriteTheDocs #general channel

# What is TheGoodDocsProject?

“Best practice templates and writing instructions for documenting open-source software,
collated from the best ideas within the documentarian community.”

Incidentally, the templates will be directly applicable for other domains too. So far, we have draft templates for:
* Overviews
* Quickstarts
* References
* Discussions
* How-tos
* Logging
* References
* Tutorials

We officially launched our first 0.1 release at the recent WriteTheDocs - Australia conference and were honoured to have ~ 50 documentarians review our templates in a two-hour "fix-it" session.

## What next?
* Over the next few weeks, we will be copying suggestions into our git issue tracker and Kanban board.
* By ~ March 2020, we plan to publish a 0.2 release, incorporating suggestions to date.
* We hope many documentarians will join us to help improve these templates.
* And then, with a bit of luck, people will start using the templates.

## Highlights from WriteTheDocs - Australia
A bunch of ideas were presented at the WriteTheDocs conference which I'd love to see captured in our templates and shared with others. These included:

### Inclusivity
Dave Parker presented an amusing and informative lightning talk using sign language, presented through his (female) interpreter. He started, "You are probably wondering why I sound like a woman? I was born deaf ..." He has since volunteered to add best practice inclusivity into our templates.

### Including international communities
Alec Clews discussed cross-cultural and cross-language inclusivity, along with challenges and potential solutions. It will be good to see these ideas woven into our templates.

### Empowering writers
Leticia Mooney talked in inspiring terms about practical techniques to empower technical writers. I've seen many dis-empowered technical writers and I'd love to see techniques to address this captured and disseminated.
Psychological safety

### Riona MacNamara challenged conventional thinking about open source communities. She discussed technical and social barriers to technical fields, and linked psychological safety, knowledge sharing and good documentation to building more egalitarian societies.

### Information architecture
Elle Geraghty discussed building information architectures, and avoiding social and technical biases. We have only scratched the surface of this topic in TheGoodDocsProject and more expertise is required.

### Storytelling
Jon Manning and Tim Nugent wove a compelling tag-team story about narrative in game design. If we can get technical writing to be as interesting as this, readers will absorb information more effectively.

### Tech Writing Workshop
Sarah Maddox presented Google's internal Tech Writing 101 workshop for Developers, which Google plan to share publicly. It will be great to harmonise this with TheGoodDocsProject.

### Release Notes
Alex Koren drew inspiration from horror stories when explaining best practices for release notes. This is worth watching, just for entertainment value - but we should also be capturing these insights into our templates.

### The pitch
Mat Walker recorded a post-conference talk about harnessing the post-conference arfterglow (password: WTD2019). At the conference, he was leading conversations about building a business case for us all to take back to organisations explaining the value we all gain by collaboratively sharing and maintaining templates, including:
* Training users to become familiar with standard documentation patterns.
* Reducing ramp-up time for documentarians.
* Supporting cross-domain expertise.

## What next for you?
Are you inspired? Want to help writers become more effective, and by extension, help share the world's knowledge? Come introduce yourself to the rest of us at TheGoodDocsProject.
