# Announcement 1

(Archived from old wiki. Last edited by Cameron Shorter, "3 years ago")

# TheGoodDocsProject Fixit Workshop

* 2019-11-11: Emailed to WriteTheDocs-Australia-2019 attendees.

Hello documentarians,

I’m hoping a bunch of you will feel inspired to join [TheGoodDocsProject](https://thegooddocsproject.dev/) FixIt workshop, 13:00 Thursday 14 November 2019 at [Write the Docs Conference, Sydney Australia](https://www.writethedocs.org/conf/australia/2019/schedule/).

We started [TheGoodDocsProject](https://thegooddocsproject.dev/) to create best-practice templates and writing instructions for documenting open source software. With our alpha release, we’ve created some common templates which we’d love people to test, break and improve.

**Topics:**

Diverse contributions encourage. We hope to cover:

1. As a user or developer or writer: _Try writing instructions for your favourite application based on one of our templates. Give us feedback._
1. As a technical writer or information architect: _Review one of our templates, try and find limitations, suggest modifications, offer sections to add, debate improvements._
1. You have another idea:_ Let’s hear it._

**Schedule:**

* 5 min: Introduction.
* 30 min: Break out into groups brainstorming topic.
* 15 min: groups present back to everyone.
* 30 min: Break out2.
* 15 min: Groups present2.
* 5 min: wrapup and thanks

**Logistics:**

*   We will have flip charts and pens for brainstorming, and
*   We will use track changes and Google Docs. (Bring a laptop.)
*   Source templates: [https://github.com/thegooddocsproject/templates](https://github.com/thegooddocsproject/templates) 

**Post Fixit:**

*   Join our [email list](https://groups.io/g/thegooddocsproject/) or [slack chat](https://thegooddocs.slack.com/).
*   Help us triage ideas into our [kanban board](https://github.com/orgs/thegooddocsproject/projects/1) and [issue tracker](https://github.com/thegooddocsproject/templates/issues).
*   [Contribute](https://thegooddocsproject.dev/contribute.html) further.

**Who are we:**

*   Cameron Shorter, Business Analyst by day, Open Source Developer and accidental Technical Writer by night.
*   Jared Morgan, Senior Technical Writer at Squiz
*   Erin McKean, Docs Advocacy Program Manager at Google
*   Felicity Brand (attending remotely), Technical Writer for Google Season of Docs
*   Sanket Totewar, Technical Writer and a few more things
*   [Others ](https://github.com/thegooddocsproject/governance/blob/master/ProjectSteeringCommittee.md)from TheGoodDocsProject
