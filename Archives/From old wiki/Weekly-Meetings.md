# Weekly Meetings

(Archived from old wiki—only the meeting notes portion. Last edited by Cameron Shorter, "1 years ago")

## **2020-12-16 US Timezone**

Present: Cameron Shorter, Alyssa Rock, Morgan Craft, Aidan Doherty

Notes:



*   Next two weeks, let’s take a break over Xmas.

Alyssa



*   Over break, planning to draft CoC Policy. Now that the discovery phase is closing, it’s time to start writing. She’s committing to having a workable draft for the CoC committee to respond to in January when everyone’s back.
*   The draft will include an enforcement plan, which is good since Google really cares about having that component. Apparently, Google also really likes the [otter-tech class](https://gitlab.com/otter-tech/coc-incident-response-workshop) enforcement class that Alyssa recently attended.
    *   Mozilla really stands out in terms of their enforcement procedures. See this: [blog](https://medium.com/mozilla-open-innovation/how-were-making-code-of-conduct-enforcement-real-and-scaling-it-3e382cf94415)
    *   This could grow in scope: “Code of Conduct As A Service.”

Morgan



*   Have been busy
*   Focus will be on hugo site
*   Looked at template workflow and replies
*   Willing to help out
*   Alyssa: Let’s define the reason and process for incubator. What is the “why”?
*   Looking to help Bryan square away Hugo Site
*   Begin Product Roadmap Planning of Tooling

Derek



*   Had questions about workflow
*   Wanting to understand why.
*   Should be covered in README or contributor guide
*   What is github actions doing? -- mentioned python / cookie-cutter
*   Github actions is from summer of code
    *   Comes from IA Guide jekyll website
*   Morgan: reason for incubator
    *   Can put templates there first, before moving into the quality 
    *   We may have a lot of other repos
    *   These should have their own repo space
*   Derek
    *   The Good Docs has become more than just templates
    *   Maybe incubation should just be for templates

Morgan



*   There will be command line tooling we want to have

Derek



*   Wanting to have a different workflow for incubator
*   Want to keep history when we move from incubator to templates
*   Note we also start in Google Docs, or a change request, or …
*   Maybe make use of github releases

Aidan



*   Maybe have main and experimental branches
*   These eventually move into main
*   Could have bleeding

Alyssa



*   Process
    *   Take idea from birth to stable
    *   What tools/process is required?
    *   Is release part of this? How do we tag this?
*   We want to offer a stable and known deadline

Bryan



*   Hugo has an archetypes folder
*   This could be a deliverable 
*   Can spin up website, templates, doc site
*   Currently we have markdown, but doesn’t include the whole package.
*   Once templates are approved, they are then copied into the archetypes
*   Our Goods Docs should be a model of that site.

Morgan



*   This aligns with DocOps concepts
*   Markdown + YAML
*   Allow other OS projects to give us their templates

Bryan



*   Like one click installer, using netlify, to create custom site
    *   Has all templates, contributors, code stuff, in a package
*   Could have a hugo version, then someone adds a jekyll version
*   How can we unjust this in

Derek



*   Github has introduced template repo
*   Can update from our templates
*   Creates faster development practices
*   Contributor / Readme 
*   Use case: Go make me a repo with all templates + hugo + archetypes 
*   Trying to something with python at moment
    *   Can do this with python
    *   Easiest start would be a github template
    *   Monitor github actions
    *   Auto open github PR

Bryan:



*   What is the best way for someone to repurpose this material?
*   Diff for each tech stack
*   “If you want a website with all best practices and templates” then click this button
*   Docsy doesn’t give you this yet.
    *   Doesn’t give guidance on CoC, Howtos, etc
    *   We can work with Docsy to help that

Derek



*   Most interested in helping create the tool chains for this
*   Using RST toolchain at this
*   This is a last mile problem.
*   What if we had a git submodule for ..

Aidan: This justifies its own initiative

Bryan



*   There is a “go module update”
*   You are mounting a project
*   Submodule mount in site builder
*   That would be cool
*   Morgan: Can we mount templates
    *   Bryan: Won’t overwrite archetype
    *   Next new would use the newest version of templates
    *   Don’t know what people use in the submitter
    *   This is unique to hugo
    *   Want to diff frontmatter and show what is missing
    *   Bryan currently does this manually
    *   There is tooling to ehlp with this

Next steps:



*   Define a roadmap for a toolchain workflow

Derek Ardolf9:41 AM

I am working on a side project to involves using Dendron, a "second brain" type project. It can generate the content with Jekyll, and they also have configurations for templates, so that you can easily create a new document following the templating standard. I'd definitely make use of the templates in this fashion. Dendron: https://www.dendron.so/

Alyssa:



*   Want to ensure we still have a focus on content plan

Morgan Craft9:44 AM

seen denron, interesting project

Morgan: has company gitbabble



*   Processes creating 
*   Engineers find it hard get engineers to write docs
*   Knowledge infrastructure - looking to open source tooling
*   Tooling 
*   Git babble API CLI is open source

Markdown, 

Spotify , backstage 

Morgan Craft9:47 AM

https://backstage.io/

Have a tooling arm and content arm

We need someone to help to define the doc template vision. 

Alyssa: wanting to get the Minimal Viable Docset for community docs.

Derek, there can be two separate teams

Getting this published, will build a community

Morgan:



*   Comes from Engineering: RFCs, …

Alyssa:



*   Install guide
*   System architecture

Cameron



*   Want to focus base template and roadmap in Jan

Alyssa: Cronologue (fictional project)



*   Should get this up
*   Marry tools and templates in action

Bryan:



*   Mount model as content with fictional project
*   Content mount
*   Then can remove content mount based on example
*   Can have a website with 
*   Don’t want to bake tools into the example project
*   Would be better to unmount fiction project
*   Can pull own and inject

Aidan



*   Work out dependencies
*   Want to be dependant
*   Can continue conversation on email list
*   Aidan to help with content. Engineer with 3 years as tech writing. Offering to help with content architecture and templates.
*   Need to help with working plan / roadmap

Alyssa



*   Will help with community docs initiative
*   Do working groups

Bryan



*   Hugo migration should wrap up soon
*   Then issues based update website
*   Want to next work on the “clone a website” toolchain

Morgan



*   Will help with tooling
*   Will help with IA guide move to hugo
*   Morgan to start on building a tooling roadmap
    *   Maybe have it as a new repo
    *   Site-template (different to templates)
    *   Not sure what the deliverable
    *   Do a RFC first

Derek Ardolf10:13 AM



*   I opened up a PR related to pre-commit tooling, in the event that we wanted to use that in our repo(s): [https://github.com/thegooddocsproject/templates/pull/175](https://github.com/thegooddocsproject/templates/pull/175)
*   Considering pre-commits
*   Wondering about cookie cuter, working with dedron

Bryan Klein10:16 AM

https://github.com/google/docsy-example

Bryan Klein10:18 AM

https://docs.netlify.com/site-deploys/create-deploys/

https://docs.netlify.com/site-deploys/create-deploys/#deploy-to-netlify-button


## **2020-12-16 APEC Timezone**

Present: Cameron Shorter, Ankita, Viraji, Chris Ward

Notes:



*   Viraji
    *   Has been looking at Alyssa’s flowchart that was brought up in the Slack discussion by Derek. Derek has raised some points for consideration. Gave my feedback from what I have worked on. 
    *   Have base template showed.
*   Welcome committee
    *   Available for mentoring: Ankita, Viraji, Cameron
*   Chris Ward
    *   Someone needs to define user experience / personas / docs
*   Ankita
    *   What are our 2021 tasks?
*   Cameron
    *   Has started working a roadmap doc
*   Plan to take a break for meetings for next two weeks over the Christmas period


## **2020-12-09 US Timezone**


## Attendees: Cameron Shorter, Aidan Doherty, Aaron Peters, Alyssa Rock, Derek Ardolf

Notes:



*   Alyssa
    *   Reading “Working in public”
    *   Attended ~ 5 hrs of CoC training, and could have gone longer
    *   It’s a big topic. Lots to think about. Sobering, but exciting!
    *   It makes a difference! Addresses larger goals such as how to make open source more welcoming for people from diverse backgrounds
    *   Has done a lot of research and now it’s time to start writing.
    *   Phase 1: Write our own CoC
    *   Phase 2: Walk through the logistical issues of setting one up, and scale.
        *   Build a Howto
    *   Contributor Covenant - Codrina?
        *   This project is noble, but it’s only covering half of the equation. It’s not enough to have a CoC, you have to have someone who takes ownership of it and makes it a priority.
        *   You need a code of conduct person or committee and work on setting a tone for the culture of your community
*   Aaron
    *   Wrapped up Season of Docs
    *   Potential to grab toolchain
    *   Plan to work through issues, oldest issues
*   Aidan
    *   Good: Heroku hosting account with slackbot
        *   Probably good for future. Free
        *   Can add people to project
        *   Pulling from git repository
            *   Could move to GoodDocs git
    *   Bad
        *   Bot gets into echo loops
        *   Bug
        *   Not sure if this is best use of time?
    *   Maybe better could be:
*   Welcome Bot (Alyssa, Derek have people)
    *   Has template responses
    *   Eg: for first pull request
    *   Eg: first commit
    *   Github actions
    *   Is there are slack version?
    *   Next step should be talk to a real person
*   We should set up a welcome committee
    *   Auto message - ask you to say hello in general
    *   Expectation is set up a 1:1 conversation with new person
        *   Typical questions
            *   What do you like
            *   What are you interested in 
    *   Action: Aidan to write up a proposal for this
*   Derek
    *   Getting oriented
    *   Looking at Alyssa’s diagram
    *   Incubator -> Templates process
    *   Suggest connect Mogan
    *   Propose a process on how to move from incubator to main repository
    *   Need to move from “As Is” to “To be”
    *   PR template
        *   Labels and/or people getting auto assigned
    *   Is incubator locked to templates? 
    *   Need to explain why we go through incubator instead of main templates.
    *   Derek to push this forward, talk with Morgan, Felicity, Alyssa in particular
    *   There are PR and Merge templates (Alyssa) to look at in github repo
*   Cameron:
    *   Suggest we focus on reviewing Viraji’s Base Template
    *   Aidan offering 
*   Oldest...
    *   Issue: [This issue](https://github.com/thegooddocsproject/templates/issues/62) describes a proposal for a Tutorial, which would be composed of multiple other documents. The discussion suggests that a Tutorial “could” consist of some number of How-To or Concept articles. While this follows logically, it begs the question of how a template of something comprised of N number of How-Tos, N number of Concepts, and potentially other connecting text could be made usable. In other words, is this truly a template? **Suggest discussing as a group how to handle composite documents overall**.
        *   Composite docs is something that we should be considering in future.
        *   This relates to [Content Reuse](https://docs.google.com/document/d/1Uo3Rcc-rRaN4kmJpqwtUaVRJmDbYI4TkwjuNWNuBu9A/edit#heading=h.t63rontlncfd)
            *   Toolchain impacts how you approach content reuse
        *   Alyssa: Tutorials is now going to approach content using modules.


## **2020-12-02 US Timezone**

Attendees: Cameron Shorter, Aidan Doherty, Aaron Peters, Alyssa Rock, Morgan Craft, Bryan Klein, Derek, 

Bryan:



*   Progress converting site over
    *   Mostly Lift and shift for the start
    *   Links work
    *   Community and contact pages merged
    *   Tracking status in Trello Board (in github)
    *   Got feedback on licensing
    *   Plan to content up to date today
    *   Should be ready for a DNS migration in a few days
    *   Would be good to have an extra volunteer to help with website
        *   Would welcome a volunteer (there are a possible few helpers interested to join the project)
        *   Could be a good job for someone new
    *   Would like a full site content review at some point after migration.

Aiden



*   Lana finished merge (didn’t need help), raised by Aidan
*   Re slackbot. On free hosting on heroku. Not working, needs debugging.

Aaron



*   Wrapping up Season of Docs
    *   Should have more time after this week
    *   Wants to collate lessons learned for toolchains
    *   WINE chapter for free-bsd handbook have good publishing pipeline.
        *   Make html split
        *   Nice pattern
        *   Feeling pains of review anti-pattern
        *   Use fabricator for all reviews
        *   Use diffs
        *   Includes stuff that is already reviewed.
        *   Git would be useful for them
        *   Using docbook (converted from pandoc). Hard to write
        *   Have indentation rules

Morgan



*   Helping with hugo migration
*   Got svg logo ported
*   Next: pick up some more pages to get project conversation across.

Alyssa



*   Codes of Conduct
*   Reviewed 6
*   Need to pick one next week
*   Writing one
*   Attending RACI and Mozilla code of conduct
*   Phase 1: We need immediate CoC for us
*   PHase 2: Create a template that we can share with others
*   Maybe make a publicity about what a good CoC is
*   Is a Keystone Doc
*   Needs to be done early on
*   Needs a story about how you can adapt it.
*   How do you maintain it
*   Eg: Choose your own adventure
*   Eg: Creative Commons have this
*   Size of project is a factor, what is expected of each role along the way
*   Audit along the way
*   It is hard for first timers to apply process
*   Tech challenges
    *   How do you report, but keep records, but keep private
    *   Need technology to fill out a form and follow up
    *   Put into a sensitive spreadsheet
    *   How do you cover repeat offenders
    *   We should create an open source project for this
    *   How do you prevent a NDA using a shield to prevent repeat offenders
*   At what point do you move from volunteers to law enforcement
*   Need an appeals process (Derek has story in FastAPI process)
*   Should do firedrills

Derek



*   Checking in to see where things are up to
*   Interested in templates
*   How do you ensure that you have updated to a future (v2.0) version of templates?
*   Looking at cookie cutter
*   Want new programming approach to Salt, eat own dogfood. 
*   Suggest look at the [base template](https://github.com/thegooddocsproject/incubator/tree/master/base-template)
*   We need a process for moving stuff from incubator to main repos. (Nudge Morgan)
    *   Should be on our marketing site, make it clear to people
    *   Alyssa developed a flow chart  - https://thegooddocs.slack.com/archives/CQT6T04CW/p1605146000106600
    *   

Notes:



*   
*   Oldest...
    *   Issue: [This issue](https://github.com/thegooddocsproject/templates/issues/59) describes a suggestion to add a “See Also” section to the How To template. Someth[https://github.com/thegooddocsproject/templates/issues/59](https://github.com/thegooddocsproject/templates/issues/59)ing to capture links to similar or otherwise related content. This type of cross-linking seems common enough in knowledgebase environments, and provided (like most sections) it’s a “use-if-you-want-and-toss-if-you-don’t,” **suggest opening this for someone to pick up**. It looks like Dishebh wanted to work on it back in May, but it hasn’t seen any movement since then… there is also no PR for it.
        *   Team decided this should be in the base template and select individual templates. Aaron to take this issue up.


## **2020-12-02 APEC Timezone**

Attendees: Lana, Viraji, Ankita, Cameron

Apologies: Jared

Notes:



*   Lana:
    *   Project report is ready [here](https://gist.github.com/Loquacity/c04ae4d7d9e3d7545f43025163d18e26) (still need to write blog)
    *   Created PR to merge contributors' guide to .github directory (https://github.com/thegooddocsproject/templates/pull/172), and fixed merge conflicts in Viraji's base template PR.
    *   Also some minor additions/fixes to IA Guide
*   Viraji:
    *   Base template PR merged
    *   Trying to register  for WtD
    *   Fixed and merged the PR for the link to her article on the Press page.
    *   Reviewed Lana’s PR on the Contributor’s guide.
*   Cameron:
    *   Preparing for WtD, plan to set up a 45min unconf session for TGDP at 4:15pm AEDT tomorrow (Thurs 3 Dec)
    *   [Halfway status - Cross-organizational glossary pilot](https://docs.google.com/document/d/1qszy0Wve9wCQ1PSdrlAGgh4gU0zLO7E8SGJvIps-5mU/edit#heading=h.lf1wki8isbbg) 
*   Ankita:
    *   Glossary updates
        *   Starting on documenting process for building a glossary
    *   Reviewed the assigned CoCs


## **2020-11-25 APEC Timezone**

(No US meeting this week due to Thanksgiving)

Attendees: Cameron Shorter, Lana Brindley, Viraji Ogodapola

Apologies: Ankita, Jared

Notes:



*   Viraji:
    *   Base template PR, feedback provided on GH.
        *   This is good to be put into the Incubator so others can see it.
        *   Action on Viraji to put back into incubator.
        *   There will be a clash with the contributors guide. Lana will help with rebasing PRs.
    *   Medium article, looks good, discussion about use of Daniele Procida's documentation types (probably shouldn’t reference it, and link to IA Guide landing page [https://thegooddocsproject.dev/ia-guide.html](https://thegooddocsproject.dev/ia-guide.html)
    *   Link to published article on Medium: \
[https://viraji.medium.com/good-docs-anyone-6a795a0751f4](https://viraji.medium.com/good-docs-anyone-6a795a0751f4) 
    *   Thanks everyone for the review - and  for the extensive line edit!
    *   Questions about markdown, look at [https://github.github.com/gfm/](https://github.github.com/gfm/)
    *   Chrome plugin for rendering a markdown file in chrome
*   Lana
    *   Published [Information Architecture Guide](https://thegooddocsproject.dev/ia-guide.html) 
        *   Should be able to edit, PR, twine, will publish automatically
        *   Uses twine, which is like markdown
        *   Action compiles file and publishes automatically
        *   Need to remember that if you update static or auto, you will need to update the other. (Semi-significant differences).
    *   Content into  [https://gist.github.com/Loquacity/c04ae4d7d9e3d7545f43025163d18e26](https://gist.github.com/Loquacity/c04ae4d7d9e3d7545f43025163d18e26) 
    *   Lana feels still it is a bit rough around the edges
    *   Image is “Book of a manual for how to write a manual”
    *   Needs to be on a static site, which doesn’t move
    *   Good Docs Project wraps up on Monday next week
*   Cameron
    *   Viraji picked up that we [our press link](https://thegooddocsproject.dev/press.html) to Ankita’s article wasn’t online yet. Reason turned out to be that reviewers had approved the [pull request](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/60), but hadn’t merged. (It is merged now).
    *   Reviewed stuff


## **2020-11-18 US Timezone**

Attendees: Aaron Peters, Cameron Shorter, Alyssa Rock, Aidan Doherty , Morgan Craft

Apologies: Felicity Brand

Notes:

Alyssa



*   Code of Conduction — initial alignment of committee goals and mission conversations occurred on the mailing list.
*   Next, get base template
    *   After that, we’ll work on how we enforce it
    *   Alyssa is enrolled in Otter Tech Code of Conduct enforcement workshop on Dec. 5. It can’t be recorded for later sharing due to the sensitive nature of the discussions. However, they said we’re free to share the material. It’s licensed under CC-Attribution, non commercial.
*   Morgan
    *   Next helping Brian with Hugo
    *   Have open source account with netlify
        *   Has limits
        *   Not sure if works with slackbock
        *   Can host with Heroku cheaply
*   Aaron
    *   Personas out, have comments
    *   Feedback will likely be added/adjusted as requested
    *   Flat out on Season of Docs (other project)
    *   Collecting list of writing tools
        *   Interesting
*   Aidan
    *   Venn diagram Tech writing / education /story telling
    *   Focusing of life, writing novel, moving
    *   Have stable code for slackbot, which puts issue up into slack
        *   Want to host on stable hosting - such as Digital Ocean
        *   Put source code into good docs repository
    *   Bryan has a slackbot on Google app engine
    *   Criteria should be free. (for sustainability requirements for open source)
    *   Aim:
        *   Easy to maintain
        *   Ensure docs align with usability and what other projects expect
        *   Alyssa happy to have a chat 
    *   Discussed whether to use Hugo short code.
        *   Markdown doesn’t have much styling options
        *   .content element (below front matter)
        *   Can add short codes which have html templates. Can add styling elements
        *   Currently tracking in Hugo Migration Kanban board
        *   Morgan offering help, knows CSS
        *   Docsy is tied to Bootstrap
        *   Next week is US holiday. 
*   Bryan
    *   Hugo Website: Converted about page.
        *   Can use hooks
        *   Can improve control
        *   Can mount another project into the head docs (and pull in docs)
    *   Process is fairly straight forward
    *   Downloaded a zip of website
*   Oldest…
    *   Issue: [This issue](https://github.com/thegooddocsproject/templates/issues/56) raises the question of prerequisites as a standard part of the How To template. A couple of suggestions are listed, from standard “you need X, Y, and Z” to a “Gotchas” section, which Lana points out is resolved with issue 54 and [a PR](https://github.com/thegooddocsproject/templates/pull/147), which in turn is already merged. However, this doesn’t resolve the issue in its entirety. **Suggest discussing as a group if anything else is required here, and either assigning the issue if so or closing if not. Don’t forget to note in the 10/20 meeting notes that this was resolved here if it does indeed get resolved.**
        *   **Aidan, Morgan, Cameron, all think that pre-req should be in base template and others as an option.**
        *   **Aaron assigned to issue, will submit a PR to add this shortly.**
    *   PR: [This PR ](https://github.com/thegooddocsproject/templates/pull/153)has conflicts that need to be resolved. I’m not sure what to do next here.
        *   Has a merge conflict
        *   Has UK/US english issue.
        *   But otherwise looks good.
        *   Action: Aiden to reach out to the raiser, say thanks, suggest how to fix, work to resolve.


## **2020-11-18 AU Timezone**

Attendees: Cameron Shorter, Jared Morgan, Viraji Ogodapola, Lana brindley, Ankita Tripathi

Apologies: Chris Ward

Notes:



*   Ankita: Recording WTD
    *   Talked with Codrina
    *   Not getting much traction from other people
    *   Action: Set up meeting with Codrina to talk next steps
*   Viraji
    *   [Template for base template](https://github.com/thegooddocsproject/incubator/tree/viraji-incubator/base-template)
    *   Waiting on feedback (hint hint Lana)
    *   Is a README file required for Base Template? 
        *   README happens at Repo level, so not required at Base Template level
    *   Emails re contributors guide - action on Lana
    *   Can I do an article? Write about experience, in LinkedIn
        *   Yes, great idea
        *   Launa - API docs, has doctopus sticker. 
        *   Enjoyed working with us
*   Lana
    *   Code of conduct intro email
    *   Followed up feedback, raised issues. Will pick up after Season of Docs if no one else does
    *   Have [PR58](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/58) in website repo to add 
        *   Publish twinery file to website
        *   Improve readability in future without understanding twinery
        *   Morgan has helped fix YAML
        *   Waiting on Morgan’s blessing
        *   Then done for Season of Docs
    *   Working on [final report](https://gist.github.com/Loquacity/c04ae4d7d9e3d7545f43025163d18e26)
    *   Code of Conduct conversations
        *   Need to be in it to fix it
*   Cameron
    *   Started [release note 0.2](https://docs.google.com/document/d/1cmgxeoYP_zTIJtbs13_UP8TPaqEfaCpLnBZm5vjVqtg/edit) which ideally someone will take to be a basis for a blog post or similar.
*   Jared
    *   Noticed [https://twitter.com/lornajane/status/1328651856806023168](https://twitter.com/lornajane/status/1328651856806023168) which was from a WTD alumni. Great to see folks sharing our project on the internet.
    *   Keeping an ear and eye out for activities relating to GSoD wrap up. 


## **2020-11-11 US Timezone**

Attendees: Cameron Shorter, Alyssa Rock, Aaron Peters, Felicity Brand

Apologies: Bryan Klein, Aiden

Notes:



*   Bryan Klein: I won't be able to attend the meeting today.  My update on the Hugo migration, the site is working as intended now, hosted on Netlify.  Issues are being created and content migration should begin tomorrow. 

    I updated the ReadMe in the GitLab project with the new URL. Preview site URL is [https://gdp-hugo.netlify.app/](https://gdp-hugo.netlify.app/) 


    this URL will be the same until we move the domain over. After content migration

*   Aiden: I won’t be making the meeting today, it’s my wife’s birthday. I’ve got the slackbot stable and I’m going to promote it to some real hosting next and invite it to our public channels. It will only work in public channels. Also the create issue functionality turned out to be infeasible. So it only provides a reference to an existing issue.
*   Morgan
    *   Have hugo repo update working
    *   Can pipeline working
    *   Porting content over works
    *   Next: Help Lana
*   Aaron
    *   Gone over feedback from personas
        *   Feedback from Viraji
        *   Talked with Lana re feedback
        *   Suggest not ready by Dec 5 deadline
        *   “What is ready”
        *   Does it belong in “The Good Docs” as it is not a template
        *   Want to look at existing solutions. Software X does Y. Put into user stories
            *   Eg try Madcap Flare, oxygen, wikis, markdown editors
            *   Find how these toolchains suit personas
            *   morgan : suggest add persona of “product”
        *   Morgan Should The Good Docs be a teacher of toolchains
        *   Morgan; setting up a git babble community
            *   Mailchimp have set up a “uni”
*   Alyssa:	
    *   Maybe start with collaborative blog post
    *   Code of Conduct issue
    *   Reading lots of Code of Code of Conducts
*   Felicity
    *    We are fiscally hosted by Open Source Collective so we could put a [GitHub sponsor button](https://docs.opencollective.com/help/collectives/github-sponsors) on our organization.
    *   Aaron: Do we want to think about the overhead
    *   Cameron: It helps if we puts to a specific goal. Eg trophy projects
    *   Probably put on ice till we have a need for money.
    *   We should keep in the back of our mind. We _could _do it if we wanted to, but we don’t want to right now.
*   Alyssa
    *   Potentially could offer a systems architecture template
    *   Felicity wants this
    *   Could create a “Howto create a template” process. Action to connect with Lana/
*   Cameron: [Release 0.2](https://docs.google.com/document/d/1cmgxeoYP_zTIJtbs13_UP8TPaqEfaCpLnBZm5vjVqtg/edit) draft
*   With triaging, we should have a “todo” column, but we don’t have an action.
*   Oldest…
    *   Issue:**Note, the meetings have gone long enough over the past couple weeks that the issues for 11/4, 10/28, and 10/20 (USA mtg) are still pending review.** Given enough time, the next oldest issue is [this one](https://github.com/thegooddocsproject/templates/issues/61) (61) that suggests explaining the difference(s) between “tutorial” and “howto” templates, specifically when you’d use one over the other. [A related issue](https://github.com/thegooddocsproject/templates/issues/77) (77), which has been closed and merged, suggests a more general explanation of all template/doc types. **Suggest discussing whether 77 satisfies the needs of 61, then closing 61 if so, or assigning 61 if not.**
*   Cameron: Suggest we focus on just one issue per meeting. Do one well, rather than many partially.


## **2020-11-11 APEC Timezone**

Attendees: Cameron Shorter, Lana Brindley, Jared Morgan, Viraji Ogodapola, Ankita tripathi

Agenda Notes:



*    Ankita
    *   Blog post written and reviewed
        *   Jared to do a grammarly check on the doc
    *   Glossary - working on concepts/terms - wrapping up
        *   Codrina: GRASS Community. Asking about time commitment
        *   Do we have time to review?
        *   Maybe get volunteers to join slack
        *   Note: Alyssa has been building .github health files, mostly 
*   Viraji
    *   Base template working on today
        *   We have a contributors file. Should we duplicate the main contributors file
        *   Lana will move contributors guide somewhere else
        *   Need to move across to main repo later and can change structure at that time.
    *   Looking at release plan
    *   Plan to have done by end of Nov soft launch
    *   Reviewed blog
    *   Lana gave review - Kudos to great article - really good
*   Lana
    *   Issues out, doing more
    *   Actioned feedback
    *   Working on final report
    *   Building list of issues and PRs for final report. Getting ducks in row.
    *   Want IA guide into the template guide. Wants help from Jared/Morgan. (Move from incubator to main report)
*   Cameron: Re pending release at Write the Docs Au/In, 4 Dec 2020.
    *   Suggest call the version 0.2, rough consensus on this
        *    questioning from Lana about whether we have moved far enough on templates to justify a 0.2 release. But we have moved forward on all the other stuff (like processes)
    *   Action: Cameron to copy email to a [release note](https://docs.google.com/document/d/1cmgxeoYP_zTIJtbs13_UP8TPaqEfaCpLnBZm5vjVqtg/) for us to collaborate around.


## **2020-11-04 USA Timezone**

 \
Attendees: Ryan Macklin, Morgan Craft, Alyssa Rock, Cameron Shorter, Aaron Peters \
Agenda Notes:



*   Ryan has been presenting on emotional personas, talking about people (via cute animals)
    *   Hoping to convince Ryan to use these personas as an initiative within the Good Docs, and help use it to shape templates, style guides, etc.
    *   [Slides](https://docs.google.com/presentation/d/1v03csaKFi_t_-dHGWQuwVmWp7BkkqAOwVKn4v4FPCBk/edit?usp=sharing)
*   Alyssa: Community health documents
    *   Created a proof of concept of github community health repo which inherits those core documents into lower repos
    *   Did experiment with that, using dummy docs
    *   Results in our repos passing github health checks
    *   We will likely want to improve these docs later
        *   Bryan walked us through trying these
    *   Cons:
        *   Physical file is not put into the lower repository making it difficult to discover the file from a user perspective. (We should open an issue with github to fix this). Action on Alyssa to open this issue.
        *   Each repo needs its own license file
    *   Need to sort out licenses and figure out how we want to handle having our community docs in a central repo
*   Alyssa: Code of Conduct Committee
    *   Assembled team
    *   Next: get templates
    *   Discussion about private vs public for discussion
        *   Suggest public, archived conversations for our process discussion in order to have community transparency. Alyssa will take to the forum to discuss.
*   Bryan:
    *   Hugo site going
    *   Worked with Erin to get stuff setup
    *   Using netify for project. Question outstanding.
        *   Do we have an account?
        *   We will need to fill out an open source form. Action on Erin
    *   Next:
        *   Iron out netlify
        *   Move content across
        *   How “Docsy” look alike do want?
        *   Instinct, keep as Docsy standard as possibly
        *   Hugo has Archetypes
        *   1. Create our site. 2. Create template site for other open source project.
        *   We might run into things we can feedback into Docsy
        *   Hugo doesn’t support AsciiDoctor well yet
        *   Morgan asking for help on getting started. Action on Bryan to provide README.
*   Morgan:
    *   heads down on other things, will review stuff
    *   Github can do tagging for issues. Really cool. Happy to help set it up.
*   Aaron: sent email eliciting feedback on [the first draft set of personae](https://github.com/thegooddocsproject/incubator/pull/11) for the Toolchain initiative.
    *   Looking for feedback
    *   Talked with Rigs to get ideas
    *   Suggest using “personas” instead of “personae”
    *   Can use Google’s [ngram tool](https://books.google.com/ngrams/) to find which term (personas wins)


*   Aidan (from prior to meeting): I think this is the oldest issue, let’s discuss: \
[https://github.com/thegooddocsproject/templates/issues/63](https://github.com/thegooddocsproject/templates/issues/63) 

    My thoughts: I would be reluctant to add a time that it takes to complete a tutorial to the tutorial template. In my experience tutorials are wildly variable in how long they take someone to compete, and I wouldn't encourage tech writers to include this. It may make some sense to add how long it takes to read an article or a page of documentation, so I might support that. But even that is debatable. My gut feeling is to let this go or initiate a broader conversation about it on Slack.

*   Ryan: For longer tutorial helps to provide indicators.
*   Morgan: (From working at a learning company. Add an estimated time, to schedule work)
*   Need to work out reading time for tech docs. Consider images.
*   Chunked concepts and provide signposting (e.g. page 4 of 7).
*   We should err toward recording the longest time. (Better to make people feel good about reading fast than feel bad about reading slow.)
*   Is there research out there on attention span?
*   What is too long for a tutorial and then break it into smaller tutorials
*   Morgan, in training company, always were breaking down long dense content into smaller concise.

&lt;Meeting ran out of time and ended here>



*   Aidan: I’m very close to being done with the feature of GDP Bot to automatically add a GitHub issue through Slack. Just having a bit of authentication trouble but I expect to have it working this week. For now, I’m authenticating using my own GitHub credentials but when we put this in production we’ll want separate GDP GitHub credentials for the Slackbot for security reasons.
*   Oldest…
    *   Issue: [This issue](https://github.com/thegooddocsproject/templates/issues/59) describes a suggestion to add a “See Also” section to the How To template. Something to capture links to similar or otherwise related content. This type of cross-linking seems common enough in knowledgebase environments, and provided (like most sections) it’s a “use-if-you-want-and-toss-if-you-don’t,” **suggest opening this for someone to pick up**. It looks like Dishebh wanted to work on it back in May, but it hasn’t seen any movement since then… there is also no PR for it. ← _I’m confused as this doesn’t seem to be the oldest issue, i would have thought templates 63 is the oldest (Aidan)_
    *   PR: [https://github.com/thegooddocsproject/templates/pull/153](https://github.com/thegooddocsproject/templates/pull/153) 

        Mostly good changes, although there is still an instance of UK spelling for favorites in the PR.



## **2020-11-03 APEC Timezone**

Present: Jared Morgan, Lana Brindley, Cameron Shorter, Ankita Tripathi, Viraji Ogodapola \
 \
Agenda Notes: 



*   IA Guide (Lana)
    *   Created static version of IA guide: [https://github.com/thegooddocsproject/incubator/pull/13](https://github.com/thegooddocsproject/incubator/pull/13) 
    *   Markdown version which is shorter given the lack of interactive pathways.
    *   Remaining tasks include some minor edits to images.
    *   Get GitHub action working once the content is moved into the final position.
    *   Publish the guides to the final location.
    *   Create a gist to produce a final report for GSOD & cleanup issues, etc.
*   Base Template (Viraji)
    *   Lana did copyedit on current content.
    *   Merged Lana’s line-edit.
    *   Incorporated Cameron’s comments into base template Google Doc for tracking purposes.
    *   This week working on the base template in GitHub with the goal to produce a draft of the template for review.
    *   Companion guide is quite a difficult document to create because its scope is quite broad.
    *   Point raised about putting a section in the companion guide to the base template that encourages template designers to substantiate any structural or design decisions with links out to patterns used from the internet.
*   Some dead links emerging in the templates - Lana to raise issue (along with other issues that arose from the copy edit in PR#162)
*   Blog post (Ankita) for Tom Johnson’s site
    *   [https://docs.google.com/document/d/1zMxvcR3MpGLlecZrntQko0t0TZiIVwMRm9OVFW5l19U/edit](https://meet.google.com/linkredirect?authuser=0&dest=https%3A%2F%2Fdocs.google.com%2Fdocument%2Fd%2F1zMxvcR3MpGLlecZrntQko0t0TZiIVwMRm9OVFW5l19U%2Fedit)
    *   Jared to review blog post
    *   Lana to review blog post
    *   Viraji to review blog post


## **2020-10-28 US Timezone**

Present: Cameron Shorter, Alyssa Rock, Felicity Brand, Bryan Klein \
 \
Agenda Notes:



*   Our roles as a tech writer
    *   Become the doc champion
    *   Cheerleader is a scalable role
    *   Can be programmers, engineers
    *   Bryan: “We all collaborate on end result”
    *   Accumulation of content
    *   Curation aspect
    *   Lifecycle management
    *   Connected with external academics, and injecting their work into our docs
    *   Felicity: Last role used term “Technical Communicator”, and like the term “Doc Ops”. Also “Dev Rel” has crossover.
*   Re base template
    *   
*   Alyssa
    *   Code of Conduct seen as high priority and current focus
    *   Get Github community health profile looking good
    *   Need .git repo with standard names in place, which inherits to other repos
    *   Doing this for Salt at the moment
    *   Plan is to copy the research from Salt’s R&D minimal viable docset project into The Good Docs
    *   Plan to share minimal viable docset
    *   Initial version shouldn’t be a heavy lift, just a case of copying an existing
    *   First main focus in CoC enforcement
    *   Should set up some mock fires to practice
    *   Propose to invite people to join CoC committee, and then also to put together the draft content. Logistics. ~ 4 to 6 weeks. ~ Mid Nov.
*   Felicity
    *   Like using the term “reader” instead of “user” for our audience. (APEC comment)
*   Bryan
    *   Working on hugo website
    *   Asked Erin to be on Hugo project
    *   Hugo migration project
    *   Backlog, todos in place
    *   Renamed hugo kanban to content for: [https://github.com/thegooddocsproject/thegooddocsproject.github.io/projects/1](https://github.com/thegooddocsproject/thegooddocsproject.github.io/projects/1) 
*   Aaron: have been fully booked up with day job (it’s almost open enrollment in the U.S. insurance market) and Season of Docs work, still looking to compare toolchain personae against what Lana has identified for her GSoD project, then send out a call for feedback.
*   Oldest…
    *   Issue:
        *   We ran out of time before coming to a conclusion on [this issue](https://github.com/thegooddocsproject/templates/issues/56) from last time.
        *   The [next older issue](https://github.com/thegooddocsproject/governance/issues/5) is a recommendation to publish the governance docs. However the files in this repo include the Code of Conduct, which Alyssa is currently leading the charge to review and revise per GitHub guidelines. There might be other new documents that come up from that come up from that review that should be incorporated in some way as well. **Suggest tabling this issue until the aforementioned review is completed and we have a good idea of the scope of governance docs — do we need a new tag for those things that are currently “on hold” but still of immediate concern (compared to “The Vault”).**
    *   Bryan: Hugo has a way to point to a repository. We should be able to use that.
    *   Alyssa: is planning to set [.github repo for standard docs](https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/about-community-profiles-for-public-repositories). Action on Alyssa to setup github standard repository and put our current code of conduct.
        *   More links: [https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/creating-a-default-community-health-file](https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/creating-a-default-community-health-file) 
        *   And [https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/about-community-profiles-for-public-repositories](https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/about-community-profiles-for-public-repositories) 
        *   The caveat is that the files won't appear in those sub-repositories as actual files. As in, they aren't literally pushed to each of them, and wouldn't be included in clones of repos people pull down locally. What it will do is auto-enable their appearance in GitHub repos as though they were locally there, in cases where the equivalent file isn't present. So, if you go to open an issue, there will be templates available to select from if they exist in the .github repo, but the templates themselve
    *   Then Bryan to try setting up within Hugo surface the standard files.
    *   [Github docs](https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/about-community-profiles-for-public-repositories) are now open source. (Cool).


##  \
**2020-10-28 APEC Timezone**

Present: Cameron Shorter, Viraji Ogodapola, Ankita Tripathi, Jared Morgan, Lana Brindley, Chris Ward \
 \
Agenda Notes: \




*   Viraji: Companion document for base template draft version done at:
    *   About Base Template: \
[https://docs.google.com/document/d/160Hirn6tMvBlndMxEcgsmbVuUJVccxTYKN6FB6Aduvw/edit](https://docs.google.com/document/d/160Hirn6tMvBlndMxEcgsmbVuUJVccxTYKN6FB6Aduvw/edit) 
    *   PR: \
[https://github.com/thegooddocsproject/incubator/pull/12](https://github.com/thegooddocsproject/incubator/pull/12)
*   Lana:
    *   Copy edited IA guide and example site. Next do the static guide.
    *   Copy edit across all templates
    *   Feels there are templates we should dump in there later, because they are missing.
    *   PR62
    *   Notes for reviewers, need to be discussed at a group level [https://github.com/thegooddocsproject/templates/pull/162](https://github.com/thegooddocsproject/templates/pull/162) 
        *   Agree to merge this.
    *   Can approve and then push other issues to the mailing list for discussion.
    *   Lana suggests template names like How To Guide should be a proper noun with capitals. Meeting room agreed. US timezone to discuss.
    *   AsciiDoc files should be copied across to markdown. Create issues to do the work.
    *   We should standardize on person reading docs to “reader” as opposed to “user”. “User” should be used for user of software. (This is Lana's personal preference, happy to discuss other options).
    *   Should we use snippets for content reuse across templates companion guides? 
*   Ankita
    *   Talked to Codrina re glossaries. She is exchanging mails with 5 to 6 people. Will get back to us after that.
    *   Reviewed OGC doc from Rob Atkinson.
    *   Preparing presentation for being a singleton writer.
    *   Completed Blog, will share soon. \



## **2020-10-20 USA Timezone**

Present: Aidan Doherty, Cameron Shorter (briefly), Aaron Peters

Agenda/Notes:



*   Morgan: Helping Lana getting PR updates through.
    *   Contributor guide is not a template.
    *   See [Github docs: Creating a community health file](https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/creating-a-default-community-health-file)
*   Alyssa: was distracted by Write The Docs. Planning to get something to review out in coming week.
    *   We should set up a .github repo which matches github
    *   Will ask Derik to help.
    *   Document Link - [https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/creating-a-default-community-health-file](https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/creating-a-default-community-health-file)
*   Cameron: It looks like Google Templates will be released under a CC-By license. This likely will mean that we will want to move to CC-By (from BSD0) to accept these templates. Note: It appears CC-By attribution can be inserted into a HTML comment, which addresses my earlier concerns about having obtrusive attribution on downstream users of templates.
*   Aaron has [pull request for personas](https://meet.google.com/linkredirect?authuser=0&dest=https%3A%2F%2Fgithub.com%2Fthegooddocsproject%2Fincubator%2Fpull%2F11) out. Email to be sent soonish.
*   Aidan: quick demo of GDP slackbot (work in progress). Get opinions on how to handle repo associated with a channel (if any). _← Aidan will develop based on the assumption that the message writer specifies the repo of the issue they want the bot to provide a link for. So for example the triggering text might be “issue/templates 50” or “issue/incubator 50”._
*   Oldest…
    *   Issue: [This issue](https://github.com/thegooddocsproject/templates/issues/56) raises the question of prerequisites as a standard part of the How To template. A couple of suggestions are listed, from standard “you need X, Y, and Z” to a “Gotchas” section, which Lana points out is resolved with issue 54 and [a PR](https://github.com/thegooddocsproject/templates/pull/147), which in turn is already merged. However, this doesn’t resolve the issue in its entirety. **Suggest discussing as a group if anything else is required here, and either assigning the issue if so or closing if not.**
    *   PR: [This PR](https://github.com/thegooddocsproject/templates/pull/150) adds one item to the checklist in the pull request template markdown doc. I’m not sure I understand our template versioning policies well enough to provide an opinion on whether this is needed or not, but it has already been approved. It was approved by Loquacity and supported by Jared, so if we accept their judgment we would just merge it. _← Morgan merged this._


## **2020-10-21 APEC Timezone**

Present: Jared Morgan, Cameron Shorter, Viraji Ogodapola, Ankita Tripathi, Lana Brindley 

Agenda/Notes:



*   Viraji’s update
    *   Sorted out the PR conflicts (PR 9 was deleted due to merge conflicts, but content was preserved).
    *   Working on the companion document for the Base Template.
    *   Some refinement required to the headings which Viraji and Lana will collaborate on if required.
    *   Updated Base Template proposal based on notes from discussion.
*   Cameron’s update
    *   At a Google conference schilling the Good Docs Project to everyone.
    *   Plan to present an unconference tomorrow to try and recruit new members.
*   Ankita’s update
    *   Wrote the first draft of the blog about the Good Docs Project, but decided it wasn’t quite there.
*   Lana’s update
    *   Refined language in contributor’s guide.
    *   Noted that Morgan provided feedback which she will need to be rolled-in.
    *   WtD Prague discussion about format of the conference.
*   Jared:
    *   Voted as a Committer member onto AsciiDoc Spec Committee
*   Oldest…
    *   Issue: [https://github.com/thegooddocsproject/templates/issues/52](https://github.com/thegooddocsproject/templates/issues/52) (Closed, as a PR was already merged)
    *   PR: [https://github.com/thegooddocsproject/templates/pull/150](https://github.com/thegooddocsproject/templates/pull/150) (didn’t merge awaiting on Morgan’s feedback)


## **2020-10-14 US Timezone**

Present: Morgan Craft, Lana Brindley, Cameron Shorter, Alyssa Rock, Felicity Brand, Aaron Peters, Bryan Klein

Agenda/Notes:



*   Language style:
    *   Untechnical writing reasoning from Lana
    *   IA Guide
        *   Assume audience doesn’t know IA principles
        *   Remember that audience will be primed effectively before they get to the interactive guide, so they know how to handle language/format
        *   Style is designed specifically to teach tech topics to non-technical people
        *   There will be a static version (which hasn’t been written yet) which will be used later, will be concise, able to be skimmed, used for reference.
        *   Requires more copy editing
    *   Contributors' guide
        *   First draft written
        *   Make style more formal/technical
    *   Base template
        *   Draft to come
    *   Felicity: key to define our audience, otherwise language choice is subjective
    *   Lana: surprised, not offended because writing style for untechnical writing was positive.
    *   Lana: Agrees IA guide is a bit wordy
    *   More time, will work on github actions
    *   Alyssa, Aaron, Bryan: happy with Lana’s points
    *   Felicity: next to talk about audience
    *   Lana: people in open source source tend to be technical, but don’t necessarily know git. But we can get you there.
    *   Cameron: [case study on QGIS audience](http://cameronshorter.blogspot.com/2019/12/why-qgis-docs-team-is-struggling.html)
*   Cameron: we should have a template business case for documentation. Why spend budget on docs rather than on another feature. Provide references and research to sell it up.
*   Felicity proposes putting our vision statement and audience somewhere easy to find. Where should that be? The website? 
    *   The wiki of the planning repo, has our vision statement and audience: [https://github.com/thegooddocsproject/planning/wiki/Audience](https://github.com/thegooddocsproject/planning/wiki/Audience)
    *   It was ported there from a GDoc: [https://docs.google.com/document/d/17YFSSPbHl7FfPh-PQjjqVM9hlCHdVjSHW6p9XcN8ODw/edit?ts=5d271232#heading=h.3wzlp1ju64p](https://docs.google.com/document/d/17YFSSPbHl7FfPh-PQjjqVM9hlCHdVjSHW6p9XcN8ODw/edit?ts=5d271232#heading=h.3wzlp1ju64p)
    *   Morgan: We need to add to the audience - engineering leaders who know see the importance of documentation. What are the best practices?
    *   Morgan: sales/product sometimes own docs, 
    *   Cameron: We should have a deliverable: Business case for docs.
    *   In open source, even though there might not be a product owner title, the role still gets done by someone
    *   Felicity will put the audience into a GDoc for folk to comment and agree. Then make a PR to put the vision and audience onto the website.
*   Alyssa proposes that we improve the [Github community profile score](https://docs.github.com/en/free-pro-team@latest/github/building-a-strong-community/about-community-profiles-for-public-repositories) for our organization by 1) either adding a .github repo or converting the governance repo to a .github repo and 2) ensuring this repo has the necessary documents and processes that Github requires. Our score right now is [uneven across repos](https://github.com/thegooddocsproject/templates/community) and [has room for improvement](https://github.com/thegooddocsproject/governance/community).
    *   Why? Because if our mission statement is to help improve documentation in open source, then we need to model this behavior ourselves. That starts by having the docs that [Github considers a sign of community health](https://youtu.be/CWfiFUNyxHc?t=4093). And, of course, let’s ensure that they are high quality docs!
    *   The heaviest lift here will be [creating a CoC](https://github.com/thegooddocsproject/templates/issues/158). We need to have more than just something that looks good in a doc. The process needs to actually work. Alyssa would like to get a small sub-committee with 2-5 people to work on this project. Would love to include Lana, Daniel, and Derek in particular, if they’re available.
*   Slackbot
    *   Bryan/Aidan working through this
*   Hugo migration
    *   Need to meet with Aaron/Rigs
    *   Bryan: Have been thrashing a bit
    *   Bryan will create a new repository
        *   Won’t break anything
        *   Hugo/Docsy
        *   Experience with Doxsy is mixed. Have needed to customise it.
    *   Mogan likes Netify. It is free.
    *   Can use different analytics
*   Oldest…
    *   [Issue](https://github.com/thegooddocsproject/templates/issues/42): The change made here seems obsolete due to later changes that were made, including renaming Discussions template to Explanations. Tentatively suggest that we close this. A related PR does seem to have been merged in June by Loquacity, so it perhaps should have been closed earlier.
    *   PR: [This PR](https://github.com/thegooddocsproject/governance/pull/8/) is for the purpose of setting up an archive for archives of Slack messages within the **governance** repository. The changes themselves are creating an **Archives** folder to house these exports, as well as a **README** to explain its purpose. **Suggest reviewing and merging this PR, as the format looks appropriate for this purpose.**


## **2020-10-14 APEC Timezone**

Present: Jared Morgan, Lana Brindley, Cameron Shorter, Ankita Tripathi

Agenda/Notes:



*   Felicity proposes putting our vision statement and audience somewhere easy to find. Where should that be? The website? 
    *   The wiki of the planning repo, has our vision statement and audience: [https://github.com/thegooddocsproject/planning/wiki/Audience](https://github.com/thegooddocsproject/planning/wiki/Audience)
    *   It was ported there from a GDoc: [https://docs.google.com/document/d/17YFSSPbHl7FfPh-PQjjqVM9hlCHdVjSHW6p9XcN8ODw/edit?ts=5d271232#heading=h.3wzlp1ju64p](https://docs.google.com/document/d/17YFSSPbHl7FfPh-PQjjqVM9hlCHdVjSHW6p9XcN8ODw/edit?ts=5d271232#heading=h.3wzlp1ju64p)
    *   Discussed this and all people at the APEC meeting agreed that the vision statement should be published on the site.
    *   Cameron suggests that the content needs a bit of a review first then publish it on the Website (don’t gold plate it though).
*   Base [template proposal](https://docs.google.com/document/d/138nKfPhoS3qTf83SrM4zOuM67EwmML__zAJEKph7s_4/edit#)
    *   Cameron: Notes that the base template proposal is good collection of ideas, but probably best to focus, and in particular I think just start writing the template, keep it simple, and then work out from there
    *   Lana, suggests we should narrow the base template a bit
    *   Lana, proposing to get into the base template, and has material which will conflict a bit. - (@Lana, from Viraji - is it possible for us to have a quick call on this sometime within the week, at a time convenient to you?)
    *   
*   Oldest…
    *   Issue: 
    *   PR: 
*   Viraji - Sorry, I missed the meeting, as I didn’t realize that the time slot has been changed.


## **2020-10-07 US Timezone**

Present: Cameron Shorter, Felicity Brand, Morgan Craft, Aidan Doherty, Aaron Peters

Agenda/Notes:



*   Welcome Aidan and Aaron into official member roles in our project.
*   GitHub access and permissions:
    *   Cameron: Seems that everyone in our groups have write access in github
    *   Felicity: Not sure how to match our roles to github access. Please help!
    *   Morgan: To investigate and give a suggestion, put in a google docs.
    *   Cameron: Look at Roles page.
*   Slackbot:
    *   Aidan: talking with Bryan, taking code.
    *   Not done yet
    *   Created repo for it
    *   Working in his personal repo while unstable
    *   Aidan’s repo for [this](https://github.com/aidanatwork/gdpbot):
    *   Our new Slackbot app (not hooked up to the service): [https://api.slack.com/apps/A01C39T14KW](https://api.slack.com/apps/A01C39T14KW)
    *   Will write docs
    *   NodeJS
*   Alyssa - settling on a time for Chronologue project, (this meeting time)
*   Untechnical writing;
    *   Felicity: unsure about audience for untechnical writing. Like the idea of making choices and being taken on a journey, but the storyline is quite long.
    *   Aidan: There is a spectrum. We shouldn’t remove ourselves from the tech setting.
    *   What is our rollout (audience?). Method. 
    *   Aaron: Based on conversation, also concerned
    *   Morgan: Put feedback on PR. Content is a bit long. Content needs some polish. Who is the audience? What is our mission. What are our boundaries? Possibly should have said a bit more in feedback.
    *   All: General concern about using “untechnical writing”. Wondering about the mandate of Season of Docs, and whether it is each person’s place to give poor feedback. We can all see Lana is passionate about where she is going and is doing a great job on the project as a whole.
    *   Cameron: If there is concern among multiple tech writers in our group, then it is better than we raise this with Lana while we are amongst friends and Lana has the chance to change direction. Better than putting out to the community and the material is just not picked up.
*   Toolchain (Aaron)
    *   Forked incubator
    *   Desktop git newbie
    *   Almost there
    *   5 personas (based on talk with Rigs)
    *   User stories - worked on
*   Oldest…
    *   Issue: [This issue](https://github.com/thegooddocsproject/templates/issues/41) proposes including the “guidelines” for using a particular template within the template itself, rather than in an “about-NNNNN.md” file. The reason for is to increase productivity, as the writer won’t need to hop back and forth between the two files. The reason against is that if a particular template type has multiple examples, this content would need to be duplicated. **Recommend discussion as a group to get input and make a decision, we can close the issue if the decision is against and close the issue/create a new issue to actually update if the decision is for.**
        *   Cameron: Two personas: 1. Open Source Developer - just fill in the template. 2. Tech Writer
        *   Cameron: This is really a question that should be picked up by the base template project which Viraji and Lana are leading.
        *   Morgan: what tooling do we want to adopt. Should pick a standardized template. We probably want some YAML metadata in top of template.
        *   Decision: Let’s close this, and let it be picked up by the core templates.
    *   Issue (from previous meeting -Aidan): [https://github.com/thegooddocsproject/templates/issues/39](https://github.com/thegooddocsproject/templates/issues/39) \
^ my recommendation is that given the consensus on the thread, I will just create a PR to switch Discussions to Explanations. If this is accepted we can close the issue. &lt;- I’ve made this change and closed the issue.
    *   PR: [https://github.com/thegooddocsproject/templates/pull/144/files](https://github.com/thegooddocsproject/templates/pull/144/files). I suggest we accept this, it’s an improvement from previous README.md. ← Done, merged.


## **2020-10-07 APEC Timezone**

Present: Cameron Shorter, Lana Brindley, Ankita Tripathi, Jared Morgan

Agenda/Notes:



*   Ankita and the Glossaries
    *   Email content reviewed. 
    *   Guest post on the Good Docs Project hosted on Tom Johnson’s post.
*   Contributor Guide first draft: [https://github.com/thegooddocsproject/incubator/pull/8](https://github.com/thegooddocsproject/incubator/pull/8)
    *   Cameron suggested that we add in a note about audience *somewhere* in the IA guide about writing style.
    *   Cameron suggested that we just make a note that writing in a “untechnical” writing style is “correct” for the audience but is not a “template” for other templates in our project. 😕

Viraji said:

We happen to have a full day power cut today (8 am to 5 pm) and I only got to know about when the power went off! I've been working on my battery till now, but I don't think I'll be able to make it to the meeting at 4 pm, my time.

Apologies!

Task update:

I went through in detail through some of the existing templates, the alpha strawman collection included, to arrive at a suitable structure for the Base template. This is still WIP, and I'm working on shareable docs (off the incubator). Working on the docs helps me compare and arrive at the final content for the incubator pages. I would actually like to share those docs with you all, for your comments, as I build up on the content. Is that agreeable by you all, or is it standard practice that we stick to incubator reviews?

Also going through Schema/structure and other references for the same.

Did some minor updates to the incubator/base template pages, mainly to ensure that I get on with a working branch - only to get stuck at the PR merging changes on my branch to the main!

Mailed Lana on the above, hoping she'll do her magic on my PR.😇

I'll continue to work on the content of the Base Template folder, and send PRs for review.

Will meet you all next week,

Lana:



*   Sounds like Viraji could do with help with github
*   Possible merge conflicts. Should connect early to git too far off.
*   Possible Viraji didn’t have a title on the pull request which made it difficult
*   Suggest Viraji jump onto slack and ask some questions for help

Jared:



*   Has contributors guide for using git which might be helpful \


Viraji - update on 08/10/2020



*   Resolved the GitHub issue, and created a PR with changes: \
[https://github.com/thegooddocsproject/incubator/pull/9](https://github.com/thegooddocsproject/incubator/pull/9)
*   Problem was because I had tried working on a Fork of the main branch, instead of a feature branch off the main one. Lesson learnt! :)


## **2020-09-30 US Timezone**

Present: Cameron Shorter, Felicity Brand, Lana Brindley, Alyssa Rock, Aidan Doherty, Aaron Peters

Agenda/Notes:



*   Felicity: Propose adding a channel to Slack for sharing links to relevant articles, reading, research that will be interesting to our project. I just don’t know what to call it. I thought “research” was too heavy, “reading” sounds weird out of context, “resources” doesn’t fit either. Suggestions please. For example, Erin found that excellent FOSS Governance collection.
    *   Topical?, Books?
    *   Do we archive our slack? (Requires a license, cost maintenance)
    *   Should we put into a website or wiki, or an archive google doc.
    *   Is there a tool which will scrape a git channel and copy into a website? Maybe over-engineering?
    *   More ideas: knowledge-share, knowledge-exchange, mutual-learning, common-learning, cross-learning, food-for-thought, mind-share, helpful-resources
    *   Action: Felicity create new channel and make a new GDoc to transfer interesting shares to, so that we can do a one-off paste into our Wiki when we need to.
    *   Bryan: has repository of tools to make a Slackbot (that makes Slack talk to GDoc). Aidan will check it out.
*   GSoD update
    *   IA Guide: [http://lanabrindley.com/twinery/test.html](http://lanabrindley.com/twinery/test.html) Now with (some) images, progress bar
    *   Base Template: Structure in incubator repo, content coming soon: [https://github.com/thegooddocsproject/incubator/tree/master/base-template](https://github.com/thegooddocsproject/incubator/tree/master/base-template) 
    *   Licence for images should be CC-By.
    *   Rigs: Check on layout of text, and contrasts. Lana: No CSS yet.
    *   Watermarks should be clearly visible, the best option is to place the watermark on a section of background, typically on the lower right corner of the image.
*   Hugo migration: 
    *   We have a [kanban](https://github.com/thegooddocsproject/thegooddocsproject.github.io/projects/1)!
    *   Felicity added existing issues to the ‘Backlog’.
    *   Bryan needs to review the cards but he reckons they seem sane.
    *   Aidan will look through the issues and see what he can grab.
    *   Erin raised a PR but it seems a bit behind, so we’ll probably use a sub-directory and put all the Hugo stuff in there. Then it can live within the current folder structure, then at migration we’ll just drop it down? a directory level and it’ll be amazing.
    *   Rigs says we can do both (a new folder and a development branch). We don’t want Hugo work to block ongoing website work. 
*   Cameron: Discussion about [Commit strategy](https://docs.google.com/document/d/1yvjqqbOc_5JqKhLnnZH2kLlyuj1r1j4sFZr8cvE5i7I/edit): permissive vs. restrictive. Permissions:
    *   Gitlab as 
    *   Github done by owner files. Can be done on a branch and folder level.
    *   We can create owners list for folder
    *   Agreement that we can be loose of access control 
    *   Action: Create extra access for website for: Bryan, Aidan.
*   Morgan
    *   Connected with Lana, looked at IA guide
    *   Action: Lana call for help: look at Lana’s github action. No rush.
    *   Github actions - question: 
*   Aaron:	
    *   Toolchain proposal, working on, next few days into incubator
    *   Looking at Write the Docs channels for “What I wish I had”
*   Chronologue
    *   Alyssa: will put out a poll for a workshop
*   Alyssa:
    *   Template in [incubator](https://github.com/thegooddocsproject/incubator/pull/7)
    *   What are steps to get into the main repo?
    *   Morgan: Doesn’t have a process yet
    *   Idea: want to prevent template pull requests getting too messy.
    *   Incubator can have tooling which powers repo
    *   Action on Morgan: We should write down this process. 
    *   Rigs: Moving between repository is to use a pull request.
    *   Can use branches
*   Oldest…
    *   Issue: [This issue](https://github.com/thegooddocsproject/templates/issues/39) describes the use of the word “discussion” to describe/label [this template](https://github.com/thegooddocsproject/templates/tree/master/discussion). At issue is whether “discussion” is the correct word to describe what this template is delivering, which is intended to “explain background information and provide context-specific knowledge on a particular topic.” **Recommend discussing as a group whether another term, such as “explanation” (as mentioned in the about file for this template) is more appropriate, and finding/replacing “discussion” as appropriate. Note this may be included in text related to other templates by reference.**
    *   [Pull Request](https://github.com/thegooddocsproject/templates/pull/140): This pull request adds a conclusion paragraph to the How-to template, but commenters including Cameron felt it could be worded better. I agree. I suggest we close with no action. We can also discuss as a group whether to raise a new pull request with Jared’s alternative wording.
        *   Added Jared's suggested text and merged.


## **2020-09-30 AUS Timezone**

Present: Cameron Shorter, Lana Brindley, Jared Morgan, Viraji Ogodapola

Agenda/Notes:



*   IA guide
    *   Showed neice’s images which feature in the updated PoC [http://lanabrindley.com/twinery/test.html](http://lanabrindley.com/twinery/test.html) 
    *   Added progress bar and back button. Did not add TOC/breadcrumbs (bit too heavy for the tool, and don't want to encourage people to move around too much, it's important that it's a linear progression)
    *   Sorted out tech issues with the compiler.
*   Base Templates
    *   PR created with proposed structure [https://github.com/thegooddocsproject/incubator/pull/7/files](https://github.com/thegooddocsproject/incubator/pull/7/files) (merged)
    *   Viraji will work on Companion guide. Lana will work on Contributor guide. 
*   WtD Aus/India talk acceptance:
    *   Lana & Ankita got accepted (non-TGDP talks, though)
*   Vale
    *   Chris: Vale has a glossary
    *   Cameron: Check out [Glossary pilot](https://docs.google.com/document/d/1Fjrl34ErnYammel9WmvXJ3rMWFANjoSiiGyyNSYOXUg/edit#heading=h.cnw43qg2wjjs)  and [slides](https://docs.google.com/presentation/d/1WZw1_iAC8uWTqFo335i5h_q8IbEYLqzNRYYWBRNtkd0/edit)
*   Oldest…
    *   Issue: [https://github.com/thegooddocsproject/templates/issues/36](https://github.com/thegooddocsproject/templates/issues/36) - Closed (out of date)
    *   PR: [https://github.com/thegooddocsproject/templates/pull/139](https://github.com/thegooddocsproject/templates/pull/139) - Merged


## **2020-09-23 US Timezone**

Present: Cameron Shorter, Lana Brindley, Alyssa Rock, Bryan Klein

Agenda/Notes:



*   Lana: after QA of [http://lanabrindley.com/twinery/test.html](https://meet.google.com/linkredirect?authuser=0&dest=http%3A%2F%2Flanabrindley.com%2Ftwinery%2Ftest.html). 
    *   Release notes:
        *   Won’t calculate critical path score yet
        *   Bullet/blank on todo list.
        *   Words & game play still needs tweaks
        *   Reasonably stable.
    *   Github action written, working with Morgan: [https://github.com/thegooddocsproject/incubator/pull/5](https://github.com/thegooddocsproject/incubator/pull/5) This will need to move to the templates repo with the IA Guide.
    *   Someone not familiar with twine/twee should be able to use simply. The source file is very similar to markdown with some basic variables.
    *   Action means every change to source file merged to repo will automatically update the live site. Reduces maintenance effort.
    *   No styling yet. Uses default CSS.
    *   Next: work on base template, working with Viraji who has base material ideas.
    *   After: static version of IA.
    *   Alyssa: Where would you like feedback: Lana: In the incubator github repo [on the pull request](https://github.com/thegooddocsproject/incubator/pull/6).
    *   Cameron: Interactive guide requires commitment to follow interactive fashion. Needs a static version too.
    *   Tone: very conversational, called "untechnical writing" (see book by Michael Bremer). About holding hands and discovering things together.
    *   Cameron: useful to have sign posting. “You are on page 4 of 10”.
    *   Investigating having 10 year old to do drawing on commission.
*   Aidan
    *   Haven’t heard back from Bryan re Hugo. Will reach out or start …
*   Alyssa
    *   Merged Style Guide into incubator
        *   Will reach out to Morgan about next steps for getting it into the main repo
    *   Chronologue spike meeting
        *   A little interest, but short on bytes
        *   Will send out a doodle poll to those who are interested to set up the meeting
    *   Glossary
        *   [Kanban board](https://github.com/orgs/thegooddocsproject/projects/3) has been started
        *   Cameron started adding tickets — yay!
        *   Anktita/Naini/Alyssa are meeting soon to decide how to divide up tasks
*   Bryan
    *   Finishing past deadlines
    *   Zero progress so far on Hugo
    *   Cloned repository
    *   Chatted where to put hugo
    *   Erin did set up an earlier Hugo
    *   Aidan/Cameron: Value is info architecture, not the styling. (Styling sits outside of our code deliverable).
    *   Content/Schema is in scope. Layout is out of scope (except for the base style required to get it up and running)
    *   Have [collaboration doc](https://docs.google.com/document/d/1IgWPP-4ON0MSk7d04Vgne0ZgWtrpQqPMOi9XwJ4hwN0/edit?usp=sharing) 
    *   Need kickoff in [Kanban](https://github.com/thegooddocsproject/thegooddocsproject.github.io/projects/1). Bryan can ask Lana, Alyssa (and maybe Felicity) have done before and help.
    *   Aidan: Can put spike questions in the Kanban.
*   Oldest…
    *   Issue: [This issue](https://github.com/thegooddocsproject/governance/issues/5) suggests adding the governance documents (which currently include the Code of Conduct and a page for the PSC to also include decision-making) should be added to the website. There has been a recent discussion on the decision process/format, but if/once this is settled these would be good additions to the site. **Recommend adding the MD files ProjectSteeringCommittee.md and CodeOfConduct.md to the website repository if/once there are no more outstanding questions on them. MERGED.**
    *   PR: [This PR](https://nam04.safelinks.protection.outlook.com/?url=https%3A%2F%2Fgithub.com%2Fthegooddocsproject%2Ftemplates%2Fpull%2F138&data=02%7C01%7Cadoherty%40telesign.com%7C405d59c387f5439e1abb08d85fdf0cb5%7Cd818b557ea1c4070a3f1928330b7a30c%7C0%7C0%7C637364758882274257&sdata=Yke0IcyEaMetek2EJi%2BdcYFMIDDWETdtS3X7fWSnJ4o%3D&reserved=0) completes the About section on the About how-to page. I recommend we merge this, it is already approved. There is a missing word (“to”) but this is an uncontroversial change and we can make it after we merge.


## **2020-09-23 AUS Timezone**

Present: Jared Morgan, Cameron Shorter, Lana Brindley, Viraji Ogodapola, Daniel Beck, Felicity Brand, Ankita Tripathi

Agenda/Notes:



*   IA Guide update:
    *   [http://lanabrindley.com/twinery/test.html](http://lanabrindley.com/twinery/test.html) is a test version of the IA guide.
    *   Mostly working with a few bugs surrounding the table towards the back.
    *   Some issues with lists that users need to populate with values.
    *   Content is mostly ready to go.
    *   Currently unstyled. Can inherit any CSS that we want to apply to it.
    *   GitHub Action created to manage builds. May need to be merged before we can see it working. Morgan to review.
*   Base Template:
    *   Viraji’s [base template proposal](https://docs.google.com/document/d/138nKfPhoS3qTf83SrM4zOuM67EwmML__zAJEKph7s_4/edit?usp=sharing)
    *   What are the deliverables? Lana to assist Viraji with this.
    *   [Critical Paths table](https://docs.google.com/document/d/1Ym1J031pkYZC6_XBQCRl4TmX98S9O3xu2I7zQjRK6o8/edit)
    *   Next:
        *   Project deliverables
        *   Timeline
        *   Template same as other templates
        *   README
        *   Companion doc on site, contributors guide
        *   IA guide and base template delivers need to connect with each other
*   [Chronologue Fake Project](https://docs.google.com/document/d/1YrwKc15_CB5y48myT3_8DhV7gGq0Rmtn0_0d0W5fd5s/edit?usp=sharing)
    *   Felicity looking for feedback
    *   Going to have a session. Join us on the #chronologue-docs channel and send Alyssa a DM to take part.
*   Hugo migration
    *   Waiting on Bryan
*   [Contribution guide](https://docs.google.com/document/d/1yvjqqbOc_5JqKhLnnZH2kLlyuj1r1j4sFZr8cvE5i7I/edit?usp=sharing)
    *   What quality criteria do we give to committing
        *   Diff for each repo
        *   Incubator, low bar
        *   Templates, high bar
        *   Lana/Daniel reviewed and happy or about to add comments
    *   How “graduate” from incubator repo to templates repo, for example.
    *   How set a framework for this workflow? Do we have to invent the wheel?
*   Cameron has presented on [TW Patterns](https://docs.google.com/presentation/d/1yFJ2WL-l8O1vnNR67bFfmzHu6tyJjtkJD-cSyH3mNes/edit) with the presentation on [https://youtu.be/yiGFbXYyCr0](https://youtu.be/yiGFbXYyCr0) 
*   


## **2020-09-16 US Timezone**

Present: Aidan Doherty, Cameron Shorter, Alyssa Rock, Aaron Peters, Felicity Brand

Style Guide: Alyssa



*   Issues resolved, ready to be merged in.

Chronologue fake project



*   4 people interested, Alyssa, Felicity, Erin, Cameron
*   Doodle poll sent
*   Should ask in the #general channel if others are interested.
*   It has become a critical path now. We need an example project.
*   Should we switch the name “chronologue to time machine”
    *   API control for software docs
    *   Runs on Raspberry Pi (this is a real thing, which maybe shouldn’t be referenced from a fake project because we may end up using too much “real” documentation)
    *   Felicity will start a terms of reference for meeting

Aidan:



*   Intro to Hugo research. Feels ready to dive in now and itching to do something. Waiting on Bryan.

Aaron:



*   Started (other) Season of Docs project - Wine FreeBSD chapter. Tip: FreeBSD is humbling for old time linux user.
*   Looking to post the list of toolchain personae and their (supposed) pain points by the end of the weekend to the incubator repo.

Felicity / Alyssa:



*   Interested in how we merge PRs (thinking about Style Guide)
*   Maybe set expectations for process.
*   Time for dragging out.
*   Does everyone have a voice?
*   What is near enough/good enough.
*   “Docs aint code” There is subjectivity.
*   There are big and small merges, should have different bar levels.
*   What is the action?
    *   Set expectations to the community
    *   Do we have lots of merge requests first
    *   Do we accept and then add fixes as pull requests later
*   Person reviewing could say “uncontroversial so I have accepted”
*   Every review has been valuable, there is a curve
*   Cost of debate.
*   Err on accepting 90%, rejecting 10%
*   Alyssa will start a doc covering details

Agenda/Notes:



*   Oldest…
    *   Issue: [This issue](https://github.com/thegooddocsproject/templates/issues/14) represents a discussion on which license to use for our output. While most in the comment thread seem satisfied that the 0BSD license (which is used and approved for documentation by Google, for example) is satisfactory from a legal perspective, the question outstanding is whether it will be confusing for users. **Recommend putting the question to the group for a vote in order to finalize our license decision and close the issue.**
        *   As it’s using the new “The Vault” tag, we’ll re-consider this in the future once Cameron’s had a chance to consult with Google’s legal team.
        *   We’ll need a process to make sure we circle back around to these “The Vault” items as well then, or else they’ll remain filtered out of this “Oldest…” process we currently have and get stale.
    *   Oldest PR (Aidan): [https://github.com/thegooddocsproject/incubator/pull/2/files](https://github.com/thegooddocsproject/incubator/pull/2/files) (I suggest we accept this PR)


## **2020-09-16 AU Timezone**

Present: Jared Morgan, Lana Brindley, Cameron Shorter

Agenda/Notes:



*   IA Guide update:
    *   Close to draft completion.
    *   Two more sections to write and a further reading section to add.
    *   Issues with compilers but there is a way forward.
    *   Toolchain for doc generation: Twinery/Twi/Chatbook. Lana having compiler issues
    *   https://github.com/thegooddocsproject/incubator/pull/3
    *   Goal is to have an interactive demo done by the weekend and up “somewhere”. 
*   Base templates update:
    *   Lana to switch back over to base template work to help Viraji.
    *   Viraji to commit draft content to incubator repo
*   [https://github.com/thegooddocsproject/templates/issues/35](https://github.com/thegooddocsproject/templates/issues/35)
    *   Has a PR merged, closed.


## **2020-09-09 US Timezone**

Present: Cameron Shorter, Aaron Peters, Bryan Klein, Aidan Doherty, Alyssa Rock

Agenda/Notes:



*   Hugo migration
    *   Bryan has forked project to his account to submit PRs as needed.
    *   Hoping to start next week (Sept. 14th) in a subdirectory of the existing project.
    *   How do we organize? Track contributors? Suggested set up a Google Doc to add names to. ([Google Doc](https://docs.google.com/document/d/1IgWPP-4ON0MSk7d04Vgne0ZgWtrpQqPMOi9XwJ4hwN0/edit?usp=sharing) and Project Kanban board)
    *   Optionally set up kanban board in web site page. 
    *   Standard set of conversion tasks to work on.
        *   Content, Layouts, Styles (See [PR for using the Docsy Theme](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/40))
    *   Phase 1 - convert existing site to Hugo (no content or design changes).
    *   Phase 2 - could be to restructure/redesign the site.
    *   Ask for permission in #general for kanban access (Requested access)
    *   Aidan offering to help ([Go Templates for Hugo Layouts](https://gohugo.io/templates/introduction/))
    *   Should we standardize on 1 sentence per line? ([Ventilate Prose](https://writetheasciidocs.netlify.app/ventilated-prose))
*   Glossary:
    *   Training video from yesterday
    *   Alyssa to put up on you tube
*   Style guide
    *   Alyssa, processing review comments
    *   Next: How do we move to main branch?
    *   Need an example. We are missing the sample fake project: chronolog/time machine project.
*   Fake product example
    *   Is the time machine simple enough for everyone to follow?
    *   Will it cover all our needs?
    *   We should define our requirements for this.
    *   Time machine has been proposed so far, but we might need to change the fake project.
    *   Aidan, is going to have a look, but careful not to overcommit
    *   Should define marketing spec
*   Aaron:
    *   [Slidedeck - toolchain](https://docs.google.com/presentation/d/1tGBgpF3ivtxS6Y4HtT5tOs-04xVr6w0VMqTiEsn8PwU/edit?usp=sharing)
    *   Talked with Rigs re personas
    *   Task: outline major personas
    *   Perceived pain points for each person
    *   Next: break into steps 
    *   Plan to put in incubator repository
*   Oldest…
    *   [Issue](https://github.com/thegooddocsproject/thegooddocsproject.github.io/issues/14): This issue references merging governance, structure, and planning repos with the main website repo. It looks like [the info referenced within](https://github.com/thegooddocsproject/thegooddocsproject.github.io/issues/14#issuecomment-663906667) is indeed present in the website repo, and Viraji updated the nav to reflect the requested structure. It only looks like this issue itself wasn’t closed. **Recommend closing this issue. _Closed._**
    *   [PR](https://github.com/thegooddocsproject/templates/pull/138): This pull adds the content for the “About the Step-by-Step Guide” section of this template. It’s been reviewed and approved by Lana. **Recommend merging unless further review is required. _Commented on need to update referenced filename (adoc -> md) and resolve typo._**


## **2020-09-10 APEC Timezone**

Present: Jared Morgan, Cameron Shorter, Lana Brindley, Viraji Ogodapola, Ankita Tripathi

Agenda/Notes:



*   Lana: IA status?
    *   I wrote more: https://github.com/thegooddocsproject/incubator/pull/3
*   Viraji: Status of research into other templates
    *   Has been reviewing the vast amount of referenced base templates
    *   Currently putting info into a private doc
    *   Should share soonish
*   Cameron: Google confirmed they will share doc-type templates.
    *   Waiting on confirmation on license to be released under
    *   Action: Googlers to remove Google specific content before sharing.
*   Alyssa: Style guide template
    *   https://github.com/thegooddocsproject/incubator/pull/4
*   Cameron: Roles
    *   Inviting new members motion with PSC
    *   Should paid SoD members be allowed to be on PSC or is it considered a conflict of interest?
*   Aidan/Aaron: Oldest PR/Bug
    *   [https://github.com/thegooddocsproject/templates/issues/16](https://github.com/thegooddocsproject/templates/issues/16) - Closed


## **2020-09-02 US Timezone**

Present: Rigs Caballero, Alyssa Rock, Morgan Craft, Bryan Klein, Felicity Brand, Aaron Peters, Cameron Shorter

Agenda/Notes:



*   Initial draft of [toolchain overview deck](https://docs.google.com/presentation/d/1tGBgpF3ivtxS6Y4HtT5tOs-04xVr6w0VMqTiEsn8PwU/edit?usp=sharing) for general review/comments
    *   Described the purpose of this deck in a general sense, as a desirable future “to-be” state that we can work towards in small steps.
*   Hugo / Docsy Updates
    *   Bryan has some background here [https://support.thunderheadeng.com/](https://support.thunderheadeng.com/) 
        *   [https://www.femtc.com](https://www.femtc.com)
    *   Rigs material to build upon: 
        *   [https://istio.io/latest/about/contribute/](https://istio.io/latest/about/contribute/)
        *   [https://istio.io/latest/about/contribute/front-matter/](https://istio.io/latest/about/contribute/front-matter/)
    *   Timeline
        *   Connect with interested participants, identify roles.
        *   Kick-off - Sept. 21st
*   Glossary git repository 
    *   Approval from those present that we set up the glossary repository
    *   Alyssa/Naini/Anika meeting to coordinate on Glossary project
*   Last issue. Rigs gave demo
    *   Looking to hold a vote on the mailing list for the Mascot to be the new logo based on [thegooddocs.github.io/issue/11](https://github.com/thegooddocsproject/thegooddocsproject.github.io/issues/11)
    *   Closed [Updated tagline](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/39), due to not achieving consensus over rewording.
    *   Aaron offered to prepare next week’s last issue, and learn github in the process. Thanks Aaron.


## **2020-09-02 US/AU/EU Timezone**

Present: Cameron Shorter, Lana Brindley, Jared Morgan, Ankita Tripathi, Viraji Ogodpola

Agenda/Notes:



*   IA architecture status (Lana)
    *   [https://github.com/thegooddocsproject/incubator/pull/3/files](https://github.com/thegooddocsproject/incubator/pull/3/files)
    *   Style: Casual and approachable
*   Template of templates
    *   Viraji planning to get on this. Starting to do research.
*   Hugo migration getting started
*   Start inviting active people into our [project roles](https://thegooddocsproject.dev/roles.html) (as per criteria)
    *   
*   Set up a glossary git repository
    *   Please vote for this issue on the mailing list if you haven’t done so already.
*   Action oldest pull request
*   Action oldest issue
    *   [https://github.com/thegooddocsproject/templates/issues/14](https://github.com/thegooddocsproject/templates/issues/14)
    *   Proposed by Lana
    *   We have 7 open PRs, but 40 open issues
    *   Gegithuts the oldest issue unblocked
## **2020-08-27 US/AU/EU Timezone**

Present: Cameron Shorter, Rigs Caballero, Felicity Brand, Aidan Doherty, Aaron Peters

Notes:



*   Proposed Write the Docs Australia/India submission:



---


**Title:** The Good Docs Project 2.0 released at Write the Docs

**Abstract:**

A bunch of us documentarians in [The Good Docs Project](https://thegooddocsproject.dev) are in skunk-works creating best practices for documenting open source software. We will  be launching our 2020 initiatives at this Write the Docs conference.

By showtime, this is expected to cover:

* Writing templates for key doc types.

* How to build your own templates.

* How to build an information architecture which incorporates these templates.

* A doc audit framework - to help assess your doc quality.

* How to apply a style guide by extending an existing one.

* Setting up and maintaining glossaries, word lists, and preferred word lists, which are derived from multiple sources.

* And likely a few bonus extras.

In this presentation we will provide a “State of the Good Docs”, presenting the highlights of each of these initiatives (lightning talk style), by the people deeply involved in it.



---




*   Google’s doctype templates
    *   Meeting on 3 Sept. A Yes/No decision won’t happen till after that.
    *   Answers to Felicity questions:
        *   Recent Write the Docs podcast was about templates. Jared mentioned The Good Docs Project
        *   A Googler was interviewed about templates, and they were talking about docs which were build from the templates we are getting.
        *   “Cleaning of Google content” means removing any Google specific terms, links to internal Google pages and the like. Will need to be done by Goolgers before releasing outside.
*   Shall we approve [processes 2.0](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/43)?
    *   Consider whether these are too tied to GSoD processes.
    *   APEC voted +1 in favour of accepting current processes - with a further iteration to address comments that have been raised.
    *   Rigs encourages people to spend time on review.
    *   Rigs, Aidan and Felicity plan to reread and provide feedback.
    *   Morgan +1 for putting out
    *   All present agreed to provide feedback by end of week. 
*   Glossary project kickoff this week
    *   Tech Writer help will be needed in defining [fields in a glossary entry](https://docs.google.com/presentation/d/1fZxe4YRXKfYU5rAjk5GPRNyDRbwqT9NV406jjX8nB4k/edit?ts=5f43b856#slide=id.g94625946e7_1_19)
*   Website
    *   Move to Hugo?
    *   Rigs in favour of moving to Hugo. Better than jekyll, more reliable.
    *   Morgan, Rigs offering to coordinate volunteers. (Volunteers needed.)
    *   Aaron, in favour. Offering to help.
    *   Channel #hugo-migration to be setup 
*   Aaron: The GoodDocs Project toolchain presentation from Aaron
    *   Aaron has slide deck getting reviewed
    *   Comments from Rigs in presentation. Good job
*   Morgan:
    *   Ongoing with templates
    *   Incubator - still on back burner
*   Latest pull request #148
    *   From Lana
    *   Simple update of table
    *   Looks good. Morgan accepted and closed.
    *   Morgan shared screen and walked through the pull request, demonstrating the new submission template.
    *   This was very helpful, and Riggs suggested we should set up a training session during one of our meetings for this. (Great idea.)
*   Aidan offered to do next week’s “Latest pull request”. (This is a good way to get to learn the project.)


## **2020-08-27 APEC/EU Timezone**

Present: Jared Morgan, Cameron Shorter, Lana Brindley, Ankita Tripathi, Viraji Ogodapola \


Notes:



*   Information Architecture status (Lana?)
    *   [https://github.com/orgs/thegooddocsproject/projects/2](https://github.com/orgs/thegooddocsproject/projects/2) (Kanban populated)
    *   [https://github.com/thegooddocsproject/planning/issues/2](https://github.com/thegooddocsproject/planning/issues/2) (Draft ToC for IA guide)
    *   Suggestion: We need front page of the website and main README to give a compelling reason to “Get Started” (i.e. Call to action).
    *   IA Guide needs to be fun and compelling. A real “page turner’ that makes you want to finish it. 
    *   A couple of minutes per page with maybe one or two questions at the end of each page to move their learning forward.
    *   [https://twinery.org/](https://twinery.org/) could help us make this experience happen, but only after we get the core content written. The notion of twinery may inspire the output of the IA guide to be less “dry” and more interactive/”less scary”.
    *   [https://klembot.github.io/chapbook/guide/](https://klembot.github.io/chapbook/guide/) is a toolkit that sits on top of Twine to give it functionality.
*   Presentations for Write the Docs Australia/India is due 31 Aug
    *   What presentation(s) should we propose?
    *   Suggest Lana proposes her own talk for WTD AU/IND 2020 describing the process of working in GSoD and selecting a project and doing it. 
    *   Maybe shoot for a Lightning Talk spot for the Glossary project.
    *   Lightning talks for each of the sub-projects (see proposed text above)
*   Google’s doctype templates
    *   Meeting on 3 Sept. A Yes/No decision won’t happen till after that.
*   Shall we approve [processes 2.0](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/43)?
    *   Consider whether these are too tied to GSoD processes.
    *   +1 in favour of accept current processes - with a further iteration to address comments that have been raised.
*   Glossary project kickoff this week
    *   Tech Writer help will be needed in defining [fields in a glossary entry](https://docs.google.com/presentation/d/1fZxe4YRXKfYU5rAjk5GPRNyDRbwqT9NV406jjX8nB4k/edit?ts=5f43b856#slide=id.g94625946e7_1_19)
    *   Could [https://docs.squiz.net/matrix/version/latest/glossary/index.html](https://docs.squiz.net/matrix/version/latest/glossary/index.html) be used as a way of testing against what the guidelines in the Glossary project prescribe as “best practice”? (Jared)
*   Website
    *   Move to Hugo?
    *   Viraji mentioned she can help with updates in the future if the Docsy/Hugo solution is accepted.


## **2020-08-19 US/AU/EU Timezone**

Present: Cameron Shorter, Rigs Caballero, Erin McKean, Naini Khajanchi, Aidan Doherty, Aaron Peters, Derek Ardolf, Morgan Craft 

Notes:



*   Action: Cameron to put motion to list re store notes in google docs, then copy to git files for archive
*   Glossaries
    *   Propose split Glossary project into a separate repository/project (+1 Erin)
    *   Important to draw line between template for glossary and OSGeo as a test case :) 
*   Website:
    *   Action: Erin to send email to the list re Website proposal using Docsy/Hugo/…
    *   Docsy/Hugo as potential website (Erin has set up an example)
        *   Easier to set up than Jekyll
        *   Currently using Jekyll/Slate.
        *   Derek, likes Hugo/Doxy, being maintained
        *   Erin: Maybe sub-theming as a template would be helpful
        *   Rigs: How to build Hugo locally: [https://istio.io/latest/about/contribute/build/](https://meet.google.com/linkredirect?authuser=0&dest=https%3A%2F%2Fistio.io%2Flatest%2Fabout%2Fcontribute%2Fbuild%2F) . Every PR gets a code review. Netlify provides rendered view. Containerized using docker. Reduces requirements. Rigs has extended Hugo, shortcuts, glossary, mouse-over, auto code-testing for examples etc. This is battle tested, stable toolchain.
    *   Erin will create a pull request for Docsy theme
    *   Derek: addresses issue #38. Our website could be a good reference site for people following our process.
    *   Github connection info: [https://github.com/istio/istio.io](https://github.com/istio/istio.io)
    *   Morgan has jekyll build working locally.
    *   Morgan will test verify rebase Viraji’s pull request
    *   Aidan: Suggests separate platform selection from content
*   Aaron: Toolchain. Putting ideas together, mapping out user journey, 4 phases:
    *   Project Administration, Drafting, Review, Publishing
    *   Will ask for review, then share in a week or two
*   Hacktoberfest
    *   Call it “Doctoberfest”
    *   Erin will check with Google Open Source Group, will get back in 2 weeks
    *   Goal: Have issues, templates to write.
    *   Swag available
*   Naini: asks what would be a good place to start getting involved
    *   Cameron suggests jumping in on one of our subprojects which interests you, links to projects kicking off in [these slides](https://docs.google.com/presentation/d/1dHANSq-zd-n-T6sk-Z3sg2iNI_CQ58JOfipvPpN4G9A/edit#slide=id.g90f9ed8c3f_0_3).
    *   Rigs, Erin, suggest reaching on our slack asking these questions too.

Agenda:



*   Intros
*   Debrief from APEC meeting
*   [Netlify for branch previews](https://github.com/thegooddocsproject/thegooddocsproject.github.io/issues/37) 
*   Volunteers for agenda updates and oldest PR updates?
*   Volunteers for decision log updates and reviews?
*   Plan to switch the default branch in projects from ‘master’ to ‘main’? See [https://opensource.google/docs/releasing/preparing/#inclusive](https://opensource.google/docs/releasing/preparing/#inclusive)
*   Hacktoberfest should we register? [https://hacktoberfest.digitalocean.com/](https://hacktoberfest.digitalocean.com/) (+1 Erin 🎃)
*   Do we want to migrate to Hugo/Docsy from Jekyll/Slate for [https://thegooddocsproject.dev/](https://thegooddocsproject.dev/)? Based on path, need to create minimal viable docs for readme and contributing to [thegooddocsproject/thegooddocsproject.github.io](https://github.com/thegooddocsproject/thegooddocsproject.github.io) (should be an extension of above Netlify conversation)


## **2020-08-19 APEC/EU Timezone**

Present: Cameron Shorter, Lana Brindley, Daniel Beck, Ankita Tripathi, Jared Morgan, Viraji Ogodapola \


Notes:



*   Wiki or GDocs for meeting minutes.
    *   Propose GDocs for main entry. Periodic copy to git file (say once a month or so).
    *   Git folder would be better than wiki (Strong opinion from Jared) +1 from Daniel, Cameron +1
*   Season of Docs proposals:
    *   Lana/Jared/Viraji are proposing to switch Lana’s Season of Docs project to being her IA Guide proposal (instead of Template of Templates proposal)
    *   Lana explains her IA guide aligns closely with Cameron’s original definition of “Template of Templates”.
    *   **Action**: Jared to request SoD admin to switch project registered. (_Sent, and awaiting response from Kass_)
    *   Critical paths: [https://docs.google.com/document/d/1Ym1J031pkYZC6_XBQCRl4TmX98S9O3xu2I7zQjRK6o8/edit?usp=sharing](https://docs.google.com/document/d/1Ym1J031pkYZC6_XBQCRl4TmX98S9O3xu2I7zQjRK6o8/edit?usp=sharing) 
    *   Design Implications:
        *   IA Guide
            *   Setting up workflows for reader to get to list of templates
            *   Considering plugin, maybe start with Google form to iframe
            *   Research [https://twinery.org/](https://twinery.org/)
        *   Base Template
            *   Deliver as top level template in repo
            *   Companion doc on site -> maybe point to repo (if not too scary)
    *   Note: Lana has started already, despite we are only in the “getting to know phase of Season of Docs”. Extra kudos, and brownie points.
    *   **Action**: Viraji granted Member access by Jared to the [https://github.com/orgs/thegooddocsproject/projects/2](https://github.com/orgs/thegooddocsproject/projects/2) Kanban to actively assist with GSoD tasks.
    *   Viraji to make a start at working on Base Template, starting off from Cameron’s doc and Lana’s initial proposal at:
        *   [Doc Framework](https://docs.google.com/document/d/1sPaVeFQFEqUs0w0sbvvWz1yW3rZd7Lba2_hNheQG25Y/edit) - notes and links (Cameron’s)
        *   [Template for Templates - Base Template](https://docs.google.com/document/d/1h-KCHxc0exhhjH8XrOTSlW_MkG6GNvzVx2BJXo9C4Hs/edit#heading=h.vz856943kz3s) (Lana’s proposal)
*   Google Templates:
    *   Cameron is pushing this inside Google, might get a big dump from Google in ~ 2 to 4 weeks that will need to be edited.
*   Glossary:
    *   Open Geospatial Consortium (standards body) has offered to have us write a blog post about our Glossary project. Ankita has offered to write it. We can base it on [Glossary management across domains](https://docs.google.com/document/d/1Fjrl34ErnYammel9WmvXJ3rMWFANjoSiiGyyNSYOXUg/edit) manifesto. Add Cameron as co-author to add street credibility (as Cameron has been quite deeply involved in these communities.) Action on Ankita to write draft, Cameron to review.
    *   [Poll for kickoff meeting](https://doodle.com/poll/6uwxyngtse4rzxsk?utm_campaign=poll_added_participant_admin&utm_medium=email&utm_source=poll_transactional&utm_content=gotopoll-cta#table) currently being voted on.
*   Kanbans
    *   Probably set up new board for Glossary project, with sub epics in there.
*   Website
    *   Viraji to attend to pull request comments, commit and request for review again. \
[https://github.com/thegooddocsproject/thegooddocsproject.github.io/issues/14](https://github.com/thegooddocsproject/thegooddocsproject.github.io/issues/14)   \


Agenda:



*   Logistics re setting up IA and Templates initiatives. (Lana)
*   Logistics re kickoff for GeoLexicon. Should build a kickoff slidedeck with owners per slide / work scope.
*   Should we [update our tagline](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/39)? Erin, Felicity, Rigs voted 0 on this.
*   


## **2020-08-12 US/AU/EU Timezone**

Present: Cameron Shorter, Alyssa Rock, Lana Brindley, Morgan Craft, Carole Baden

Agenda:



*   Introductions
*   Debrief from 
*   Debrief from Write the Docs Portland
*   Round the room update on status

Notes:



*   GSoD:
    *   Planning underway, the GSoD projects are focused mostly on making the templates more useful: base template, IA Guide, auditing.
    *   Kanban board: https://github.com/orgs/thegooddocsproject/projects/2
    *   Planning doc: https://docs.google.com/document/d/1Ym1J031pkYZC6_XBQCRl4TmX98S9O3xu2I7zQjRK6o8/edit?usp=sharing
*   Write the Docs Portland: Led by Alyssa, Clarence, Derek
    *   Help on style guide templates
    *   Lots of feedback
    *   20 or so people have joined slack channel
    *   Lots of examples templates
    *   Plan to push into incubator
    *   Clarence suggested one day hackathon for Chronologue fake project (a time machine project). Need a way to create good examples of templates. Fake project is better than exemplar project which may change over time, or creates tension between projects.
*   Morgan, auditing issues to find tags
    *   28 labels: Decisions and Improvement used a lot
    *   Good First Time issue
    *   Contributor guide: We should state how should you use labels
        *   Morgan will update this
    *   Next to focus Incubation Process
*   GeoLexicon project: [Manifesto for cross-domain glossary management](https://docs.google.com/document/d/1Fjrl34ErnYammel9WmvXJ3rMWFANjoSiiGyyNSYOXUg/edit)
*   Aaron: Tool Chain
    *   Putting together thoughts
    *   Wants to reach out to Derek

Summary of current initiatives: For August - December 2020, the following initiatives are being targeted, mostly by volunteer tech writers from the community:



*   [Refine best practices templates](https://docs.google.com/document/d/1h-KCHxc0exhhjH8XrOTSlW_MkG6GNvzVx2BJXo9C4Hs/), starting with a template for templates. (This will have a tech writer sponsored by Google’s Season of Docs.)
*   Develop an [information architecture guide](https://docs.google.com/document/d/1JaiEJnSN0-5VSA2xYPzS1XrUjdzau3m4DwIeZd4EfOo/).
*   Develop an [docs audit framework](https://docs.google.com/document/d/1e6Jc6zh-aMiEKP3BCRb5IVy4Y_FCzugYINeMcJqO9ds/).
*   Run a [cross-domain management of glossaries](https://docs.google.com/document/d/1Fjrl34ErnYammel9WmvXJ3rMWFANjoSiiGyyNSYOXUg/) pilot.
*   Build a [how-to for customizing a style guide](https://docs.google.com/document/d/1HxtaiayAJZvF0ZfNjLvRH3vYMvGTEki_TK8hFilQNJ0/)

And possibly:



*   [Implementing a style guide in tools](https://docs.google.com/document/d/10A9CuSiPTtSV6zseNNZN3l8Chs6wfDCqTiO8ur-tcDY/)
*   [Integrate doc tool chains](https://docs.google.com/document/d/1AI-i1LZUouds0lWmAoLkqff63IANokTBQ_OEgiJDENQ/)
*   Convert doc template formats from our markdown to other wiki formats

The big picture ideas: [Tech Writer Patterns and Tasks](https://docs.google.com/document/d/1Uo3Rcc-rRaN4kmJpqwtUaVRJmDbYI4TkwjuNWNuBu9A/edit)

Erin's Write the Docs presentation about The Good Docs Project: [Documentation Templates for Fun & Profit with Erin McKean](https://www.youtube.com/watch?v=FaJIAorSb34)


## **2020-08-12 APEC/EU Timezone**

Present: Cameron Shorter, Jared Morgan, Ankita Tripathi, Viraji Ogodapola, Lana Brindley, Kat Christofer, Daniel Beck

Agenda:



*   Round the room update on status
    *   Templates update
    *   GeoLexicon update

Notes: 



*   Lana walked through [GSoD 2020 - Information & Project Plan](https://docs.google.com/document/d/1Ym1J031pkYZC6_XBQCRl4TmX98S9O3xu2I7zQjRK6o8/edit#heading=h.806v33101fbw)
*   Lana has set up [Kan Ban](https://github.com/orgs/thegooddocsproject/projects/2) for GSoD and related projects
*   [Pull request for web site from Viraji](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/35). Daniel will look into working out how to build readthedocs locally.
*   Cameron will be giving presentation to Tech Comm NZ tomorrow about [Tech writing patterns and anti-patterns](https://docs.google.com/presentation/d/1yFJ2WL-l8O1vnNR67bFfmzHu6tyJjtkJD-cSyH3mNes/edit#slide=id.p) 
*   Cameron one step further along getting Templates from Google into the open - have been asked to write a business case.


## **2020-08-05 US/EU Timezone**

Present: Cameron Shorter, Alyssa Rock, Aaron Peters, Rigs Caballero, Derek Ardolf

Notes: 



*   Glossary
    *   Rigs suggested use Pandoc
*   Templates
    *   Derek interested creating a template publishing toolchain.
    *   Rigs suggest using Pandoc
    *   Derek thinking about doing this in Write The Docs - Portland
    *   Suggest defining howto use pandoc
    *   Suggest giving back to pandoc if needed
    *   Aaron volunteering to help out. Suggests Chris might want to help.
    *   Rigs offering to do a code review
*   Publishing tools: Doxy, Hugo, 
*   Aaron: Working with Chris to detail out a tool(chain) to help ease the learning curve for new contributors while still satisfying all the requirements of the project (e.g. version management in Git)
    *   The first step will be outlining a “typical” documentation process. Once we identify the steps and their participants, we can examine how they’re accomplished today, and try to find gaps/pain points.
*   Rigs/Cameron discussed how we can move Google forward. (Likely delays being anticipated for ~ 4+ weeks)

## 2020-07-29 US/AU Timezone
Present:   
Agenda: 
* Budget process update
* thegooddocsproject.dev domain renewal -- DONE
* Standing last item: [Oldest PR](https://github.com/thegooddocsproject/chronologue/pull/1) and ([same day](https://github.com/thegooddocsproject/incubator/pull/2))

## 2020-07-22 US/AU Timezone
Present:   
Agenda: 
* Budget process update
* thegooddocsproject.dev domain renewal
* Standing last item: [Oldest PR](https://github.com/thegooddocsproject/chronologue/pull/1) and ([same day](https://github.com/thegooddocsproject/incubator/pull/2))

## **2020-07-22 APAC/EU Timezone**

Present: Cameron Shorter, Jared Morgan, Lana Brindley, Daniel Beck, Ankita Tripathi, Viraji Ogodapola



*   Communicating iterative updates about progress made in Season of Docs
    *   Jared suggested that we should try and line up a “Early Access” preview in time for Write The Docs Australia 3-4 December 2020: [https://www.writethedocs.org/conf/australia/2020/](https://www.writethedocs.org/conf/australia/2020/)
    *   Daniel suggested that we should also try to release updates through email/socials every time we approve a pull request.
    *   Daniel suggested we could use the GitHub release tag feature to create a draft release note for each tag. Daniel to add an item to the PR checklist to update the draft after merges.
    *   Want to define deliverables for an end date release, probably around Feb 2021, so we have something to aim for.
    *   Suggested we use our high level Kanban to track high level deliverables.
*   Lana created a proto-plan. There is quite a lot of variation between the different project proposals, this is an attempt to align the vision and give it a timeline. Please feel free to edit as you see fit.
    *   [https://docs.google.com/document/d/1Ym1J031pkYZC6_XBQCRl4TmX98S9O3xu2I7zQjRK6o8/edit?usp=sharing](https://docs.google.com/document/d/1Ym1J031pkYZC6_XBQCRl4TmX98S9O3xu2I7zQjRK6o8/edit?usp=sharing)
    *   Need to decide on a timeline for getting v1.0 ready
*   Cameron is going to try and get some seed Templates from Google. In particular, templates that were used to create [https://cloud.google.com/spanner/docs](https://cloud.google.com/spanner/docs) 
*   We agreed to continue this APAC/EU weekly meeting timeslot. Wednesdays: https://www.timeanddate.com/worldclock/meetingdetails.html?hour=10&min=30&sec=0&p1=240&p2=47&p3=44&p4=389&p5=37&p6=136


## **2020-07-15 US/AU Timezone**

Present: Cameron Shorter, Daniel Beck, Aaron Peters, Morgan Craft, Erin McKean



*   SoD report
    *   15ish applications, some not detailed
    *   Followon proposals from donations 
    *   Template of templates/IA guide from Lana -- both or one, seems like both? Need to follow up to figure out which will go first
    *   Next to work on Daniel’s checklist project
    *   Erin to follow up with OC donation 
*   Last call for comments on [budget process](https://docs.google.com/document/d/1upcuwvc1BuvrMFqtcekBIs_ygjR91G40oOpPf3jOnxE/edit?ts=5ee2b634)
*   Standing last item: [Oldest PR](https://github.com/thegooddocsproject/chronologue/pull/1) and ([same day](https://github.com/thegooddocsproject/incubator/pull/2))
    *   merged #149 template
    *   Contributor guidelines needs update for incubator repo -- @Morgan to raise issues
    *   Needs docs around labeling
        *   Morgan round up existing issues and make recs
        *   First in template repo, then move over to incubator
*   Move governance to website -- Erin will open an issue unless someone else gets there first :) 
*   Aaron sent note to Chris about his proposal (about Vale)
*   Aaron pick up README issue?
    *   README in root rather than folders (Morgan feels strongly about this :)) 
    *   [https://en.wikipedia.org/wiki/README](https://en.wikipedia.org/wiki/README)

## **2020-07-08 US/AU Timezone**

Present:

*   Cameron Shorter, Morgan Craft, Lana B., Aaron Peters, Daniel Beck, Erin McKean, Rigs Caballero

Highlights:

*   Talked about much of the content from the AU Timezone meeting
*   Cameron is suggesting we invite some more people into becoming mentors
    *   Morgan volunteered to be a mentor to anyone looking for more technical help and upskilling
*   Rigs has joined, and has approval from his manager to put part of his 20% Google time toward The Good Docs Project :)
*   Morgan to review Daniel’s proposal and look to find alignment to some of his work from gitBabel being part of the larger eco-system.
*   Discussions of proposals (Lana, Daniel, Aaron)
*   Aaron Peters is interested in helping Chris Ward in a volunteer capacity. (Aaron is a Business Analyst.) Cameron suggested Aaron reach out to Chris and offer his services. 

Proposed Agenda:

*   Standing last item: [Oldest PR](https://github.com/thegooddocsproject/governance/wiki/Weekly-Meetings)


## **2020-07-08 AU/EU Timezone**

Present:

*   Cameron Shorter, Lana, Daniel, Chris, Ankita

Apologies:

*   Jared Morgan

Proposed Agenda:

*   Season of Docs proposals (due this week)
    *   [Proposed project to integrate tooling](https://docs.google.com/document/d/1AI-i1LZUouds0lWmAoLkqff63IANokTBQ_OEgiJDENQ/) from Chris Ward
    *   [Proposal to implement a docs audit framework](https://docs.google.com/document/d/1e6Jc6zh-aMiEKP3BCRb5IVy4Y_FCzugYINeMcJqO9ds/), from Daniel Beck.
    *   [Template for Templates proposal](https://docs.google.com/document/d/1h-KCHxc0exhhjH8XrOTSlW_MkG6GNvzVx2BJXo9C4Hs/edit?usp=sharing) from Lana Brindley
    *   [Information Architecture Guide](https://docs.google.com/document/d/1JaiEJnSN0-5VSA2xYPzS1XrUjdzau3m4DwIeZd4EfOo/) proposal from Lana Brindley
*   Migrating test the docs (Chris Ward)

Highlights:

*   Lana points out that Information Architecture should come before templates
*   Daniel notes that it will help to have Information Architecture to reference from his checklists.
*   Chris notes that Cameron’s suggestion of a Google front end into a git backend (or similar) is probably harder and more problematic than could easily be scoped in a Season of Docs project. 
*   Chris is planning to put together another proposal which will be closer to his experience which is based around helping projects implement tools behind a style guide - including click selection of a pull down list of style guides (or similar). This would be based on material he has access to.
*   Chris has style guides he wants to bring into The Good Docs Project repository. He will need git admin access for this. Action on Cameron to work with Chris to set this up when Chris is ready, assuming committee agree to accept the material.

## 2020-07-01
Present: Lana B, Cameron Shorter, Alyssa Rock, Morgan Craft, Aaron Peters, Erin McKean, Daniel Beck

Proposed Agenda:

*   update from AUS meeting?
    *   SoD: should have topics up and polished, make them look awesome
        *   Lana has added work on templates and IA
        *   Daniel & Chris Ward to add as well
        *   Is there anything else that people are interested in?
            *   Morgan/Alyssa? Updates on goals
                *   Alyssa -- working on fictional Chronologue stuff
                    *   Need bootstrap documentation mechanism -- invited Derek to join (on vacation for next two weeks) wizard about toolsets
                    *   Syncspace docs bootstrap to Chronologue
                    *   Set up Slack channel for #chronologue-docs (done)
        *   Jared’s question: definition of ‘done’ for next release
            *   Can we use SoD to define?
                *   Projects: MVP template set?
                *   Daniel’s def of quality and checklist?
                *   Chris Ward sorting out WYSWYG + Git versioning?
            *   Can we just use semantic versioning and YOLO?
                *   Can we do 1.0 at end of SoD?
                *   Is it 1.0 when we have examples for each templates? What’s the checklist?
*   Morgan: where do proposals land: incubator repo?
    *   Do update to PR template
    *   We still need issue templates for these
    *   Workflow/docops tooling -- want to contribute here
    *   Interested to see what Chris Ward is working on
*   Aaron: also interested in plumbing Google Docs to Github
    *   Have put high-level thoughts in the project doc
    *   Editor-agnostic approach
    *   Good point brought up: Needs to align with personas, can pitch on that as step 0?
    *   Should get proposal put together if want to do SoD
    *   More interested in volunteer, tooling not great fit for SoD
    *   Maybe alternative funding coming in
    *   Check in w/Chris Ward?
*   ~~SoD updates?~~
*   [Budget process](https://docs.google.com/document/d/1upcuwvc1BuvrMFqtcekBIs_ygjR91G40oOpPf3jOnxE/edit#) update (@emckean)
    *   Rockstars and plumbers: find all the possible plumbers! 
    *   Separate out the conflict resolution to a different process 
*   Incubator
    *   Anyone can do a PR!
    *   Playground
    *   Then move work to template repo
    *   Bigger proposals for things that need project management -- maybe inside governance? Things with acceptance criteria
    *   Checklist for whether you should need a proposal -- 
*   Standing last item: [Oldest PR](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/31/files)
    *   Closed, left comment
*   [Make Sense of Any Mess ](http://www.howtomakesenseofanymess.com/)

## 2020-06-24
Present: Will Kelly, Cameron Shorter, Erin McKean, Daniel Beck 


Proposed Agenda: 
 * Wrap up Morgan’s PR => Daniel to send changes 
 * Will Sharing template progress
   * Will send to list!
 * Request for list for template-testers
   * Put on hold until Github rolls out discussions 👍
 * Ping Erin w/mailing address if you want Doctopus stickers! 
 * Standing last item: Oldest PR
   * Email to Lana out for discussion  
   * Cameron to close PR

## 2020-06-17 AU/EU Timezone
Attendees: Cameron Shorter, Lana Brindley, Chris Ward

* Lana has been knocking over a number of our backlog of issues. Thanks. It would be helpful for her to have more access so she could claim an issue before working on it.
* Which leads into conversation around us needing to do a better job of defining our job roles within the project, so we can set up access to people like Lana. (Hanging action). Suggested Lana mention these challenges on list to encourage us to do something about it. She is welcome to offer to lead it too.
* Talked about TheGoodDocsProject not really having a “Howto get started process”, and that this would be a good Season of Docs initiative, which would dovetail with an Information Architecture type project. This doc is sort of the README, or index page, which ties in with other pages. Hopefully Lana will start floating some of these ideas around.
* Touched on our decision making process, and that we probably should move this out from the principles doc into our Decisions making process. 

## 2020-06-17 US/AU timezone
Present: 
* emckean, Cameron Shorter, Alyssa Rock, Daniel Beck  Morgan Craft, Will Kelly

Proposed Agenda: 
* Budget Approval [Principles](https://docs.google.com/document/d/1p-Cc44saQDt1BECXCNwwlQDFzaDE-WRPp0R-P5zMB1Q/edit) and [Process](https://docs.google.com/document/d/1upcuwvc1BuvrMFqtcekBIs_ygjR91G40oOpPf3jOnxE/edit?ts=5ee2b634)
* Decisions:
  * [Current decision process](https://github.com/thegooddocsproject/governance/blob/master/ProjectSteeringCommittee.md#decisions)
  * [consensus model](https://en.wikipedia.org/wiki/Consensus-seeking_decision-making)? As used by [NodeJS](https://nodejs.org/en/about/governance/)
* Progress: [style guide template](https://docs.google.com/document/d/1HxtaiayAJZvF0ZfNjLvRH3vYMvGTEki_TK8hFilQNJ0/edit)?
* Progress: [PR Template](https://github.com/thegooddocsproject/templates/pull/149)
* Standing last item: [Oldest PR](https://github.com/thegooddocsproject/governance/pull/7)

Notes:
* Budget Approval Principles and Process
   * Erin to revise process and give a heads-up when ready for review
* Decisions:
   * Current decision process
      * consensus model? As used by NodeJS
      * Think about and discuss at future meeting?
* Progress: style guide template?
   * What’s Chronologue? github.com/thegooddocsproject/chronologue
* Where do templates live? Embedded or linked?
   * Linked so not two places for templates
* Progress: PR Template
   * Ready to go
* Update contributor -- point people to issues
   * Morgan to take this on :) 
* Standing last item: Oldest PR
   * Cameron to review
* Next week: Will on the templates of templates 


## 2020-06-10 US/AU timezone
Present: Erin McKean, Will Kelly, Morgan Craft, Alyssa Rock, Ashley Newton, Cameron Shorter, Aaron Peters, Lana B

Proposed Agenda:
* Notes from Australian time meeting: Cameron to send notes 
* updates on [Style Guide](https://meet.google.com/linkredirect?authuser=0&dest=https%3A%2F%2Fdocs.google.com%2Fdocument%2Fd%2F1HxtaiayAJZvF0ZfNjLvRH3vYMvGTEki_TK8hFilQNJ0%2Fedit%3Fusp%3Dsharing) from Alyssa
   * next step is to clean up comments and make clean doc 
   * use project as trial run for testing the split between instructions to user of template and instructions for readers of the docs 
   * note: using HTML comments won't render on Github
   * need a template for templates -- METADOCUMENTATION ❤️ 
* discuss [Membership permissions structure on GitHub](https://thegooddocsproject.groups.io/g/main/message/126)
   * Possible solutions: possibly Apache model? 
   * PSC: decisions about overall direction of project. 
   * Also throwing budget decisions into the mix -- that adds risk 
   * Subgroup for budget decisions who answer to project steering committee?  
   * level below PSC: technically adept people to be committers -- can approve PRs. 
   * PSC should nominate, then approve (at least a month of good commits, active time in project). 
   * should have minimum quantitative bar of activity, regular review by PSC to add new committers (monthly? quarterly?)
   * at least three months activity to be on PSC  
   * anyone can raise PR/join email list 
   * Erin to write up and send email proposal :)  
* Standing last item: [oldest PR](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/29) CLOSED 
* next time -- set up way for people to volunteer to take notes? set Slack reminder for meeting agenda on Tuesdays :) 
* we're out of `good-first-issue` and `help-wanted` tags ... need to make more 
* Morgan working on PR template template :) and will look at one for the incubator repo too! 

## 2020-06-10 AU/EU timezone
Present: Cameron, Lana, Jared, Chris, Daniel

Some great conversation about long term vision for the project, but sorry, my notes are pretty crappy.
* Introduction from Lana, who is interested in Information Architecture
* Chris Ward has a bunch of material and tools (such as linter bits) which he reckons is looking for a home and The Good Docs Project might be a good place for it. 
* Discussed whether tools should fit under The Good Docs Project's scope.
* Led to noticing that the [Tech Writer Patterns](https://docs.google.com/document/d/1Uo3Rcc-rRaN4kmJpqwtUaVRJmDbYI4TkwjuNWNuBu9A/edit?ts=5ee101a3) doc is good for helping people focus just on one component. So we don't have to exclude a section, we just put it into a different working group, nicely bounded by the definition of the problem being solved.
* Suggestions about reaching out getting visibility by reaching out to high profile organisations, and in particular open source foundations. Lana had some ideas here.
* Actions: Lana and Chris and going to look around a bit more to find out where they might fit in.

## 2020-06-03
Present:

* Daniel Beck 
* Will Kelly 
* Cameron Shorter 
* Alyssa Rock 
* Erin McKean 
* Morgan Craft 
* Aaron Peters 
* Clarence Cromwell

Apologies:
* Amanda

Proposed Agenda:

* Pick one of the topics from our [wishlist of tasks](https://docs.google.com/document/d/1Uo3Rcc-rRaN4kmJpqwtUaVRJmDbYI4TkwjuNWNuBu9A/edit)
* Review of [Style Guide proposal](https://docs.google.com/document/d/1HxtaiayAJZvF0ZfNjLvRH3vYMvGTEki_TK8hFilQNJ0/edit)
   * put out for comments (2-3 weeks) then Markdownify :) 
* Who will have permission to approve budget spending? Cameron suggests long term it be subset of our current Project Steering Committee. This will allow us to grow our Project Steering Committee, without having to worry about screening for bad actors.
   * will do by email; Erin to try to find examples from other OSS
* Connect with SUSTAINOSS Docs working group [their last meeting](https://discourse.sustainoss.org/t/the-good-docs-wg/302/10). Action on Erin to do introductions.
* Standing last item: [oldest PR](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/6)
   * Closed as our project has moved forward. Action on Cameron to revisit website pages.

## 2020-05-27
Present:
* Will Kelly
* Amanda
* Morgan Craft
* Cameron Shorter

Proposed Agenda:
* ~Progress on Style Guide conversations (Alyssa)~ Postponed
* ~Status of Open Collective (Erin)~ Postponed
* Draft [Bounty task list](https://docs.google.com/document/d/1Uo3Rcc-rRaN4kmJpqwtUaVRJmDbYI4TkwjuNWNuBu9A/edit#)  (Cameron)
* Other ideas?
* Standing last item: [oldest PR](https://github.com/thegooddocsproject/templates/pull/113)

Notes:
* Will and Amanda introduced themselves and we discussed Season of Docs ideas with them.
* Yes, the bounty list could be covered by Season of Docs, or by separate Bounty sponsorship - although a process hasn't been worked out yet.
* Will suggested the term "Reverse Engineering" for the task tech writers are often performing.
* Morgan is providing feedback on the oldest pull request, which we have decided to close.


## 2020-05-20
Present:
* Cameron Shorter, Aaron Peters, Daniel Beck, Morgan Craft, Erin McKean

Proposed Agenda:
* Discuss consolidating our Project Steering Committee
* ~Progress on Style Guide conversations (Alyssa)~ moved to next week
* Season of Docs status
* [Building a list of bounty projects](https://docs.google.com/document/d/1sPaVeFQFEqUs0w0sbvvWz1yW3rZd7Lba2_hNheQG25Y/edit#heading=h.eezg939gy9vx)
* Should we move this agenda to Google Docs?
* Standing last item: [oldest PR](https://github.com/thegooddocsproject/templates/pull/91)

Notes:
* want to have ease of note-taking but also save notes in the wiki -- share Google Docs notes and then transfer
* want to have a minimum bar for Season of Docs applicants, but have it be *really* minimum 
* look at Wikimedia requirements for SoD page: https://www.mediawiki.org/wiki/Season_of_Docs/Participants
* closed PR and raised [new issue](https://github.com/thegooddocsproject/templates/issues/133)
