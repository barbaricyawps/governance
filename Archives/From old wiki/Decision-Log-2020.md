# Decision Log 2020

(Archived from old wiki. Last edited by Cameron Shorter, "2 years ago")

----------------------------------------------------------
| Raised     | Motion | Status  | Comments |
| :--------- | :----- | :------ | :------- |
| YYYY-MM-DD | [Title](https://link.to.email) | [Passed](https://link.to.email)/ [Failed](https://link.to.email)/ Pending/Postponed YYYY-MM-DD | Comments (Optional)
| 2020-10-26 | [Jo Cook has retired herself from the PSC](https://thegooddocsproject.groups.io/g/main/message/525?p=,,,20,0,0,0::relevance,,Stepping+back+from+PSC,20,2,0,77773220) | | Jo has other opportunities she is pursuing. Thanks so much for helping kickstart this project
| 2020-10-08 | [New Committers Aaron and Aidan](https://thegooddocsproject.groups.io/g/main/topic/inviting_aaron_peters_and/77370633) | Approved by PSC members | Committer: Aaron Peters and Aidan Doherty
| 2020-09-11 | [New PSC/Committer members](https://thegooddocsproject.groups.io/g/main/topic/76775222#486) | Approved by PSC members | Project Steering Committee: Morgan Craft, Alyssa Rock. Committer: Lana Brindley, Daniel Beck, Viraji Ogodapola
| 2020-08-31 | [Approve Processes 2.0](https://github.com/thegooddocsproject/thegooddocsproject.github.io/pull/43) | Approved by PSC members + number of community members |
| 2020-08-20 | [Meeting minutes to be edited in Google Docs, periodically archived in wiki](https://thegooddocsproject.groups.io/g/main/topic/motion_meeting_notes/76296935?p=,,,20,0,0,0::recentpostdate%2Fsticky,,,20,2,0,76296935) | [Passed](https://thegooddocsproject.groups.io/g/main/topic/motion_meeting_notes/76296935?p=,,,20,0,0,0::recentpostdate%2Fsticky,,,20,2,0,76296935) | 
| 2020-05-19 | [Create standing weekly agenda item to discuss the oldest PR](https://thegooddocsproject.groups.io/g/main/message/337) | Passed | 
| 2020-05-18 | [Create 1 Password for Open Source account](https://thegooddocsproject.groups.io/g/main/message/336) | Passed | 
| 2020-05-17 | [Create wiki page for weekly meeting agendas](https://thegooddocsproject.groups.io/g/main/topic/74215471#334) | Passed | 
| 2020-05-14 | [Create 'incubator' repository for new template proposals](https://thegooddocsproject.groups.io/g/main/message/322) | Passed | 
| 2020-05-14 | [Create 'incubator' repository for new template proposals](https://thegooddocsproject.groups.io/g/main/message/322) | Passed | 
| 2020-05-01 | [Join Open Collective to help fund template creation](https://thegooddocsproject.groups.io/g/main/topic/73207305#304) | Passed | 
| 2020-11-02 | [Update Governance to include Roles:community, users, contributors, committers, PSC](https://thegooddocsproject.groups.io/g/main/message/249) | Passed | 

