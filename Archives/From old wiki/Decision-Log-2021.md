# Decision Log 2021

(Archived from old wiki. Last edited by Cameron Shorter, "1 years ago")

----------------------------------------------------------
| Raised     | Motion | Status  | Comments |
| :--------- | :----- | :------ | :------- |
| YYYY-MM-DD | [Title](https://link.to.email) | [Passed](https://link.to.email) | Comments (Optional)
| 2021-03-30 | Clarence Cromwell has retired himself from the PSC | | Thanks so much for helping create the first set of templates.
| 2021-02-25 | Alyssa Rock to take over PSC chair from Cameron Shorter | Passed by private PSC vote | Thanks Cameron for prior serving as a chair. 
| 2021-02-25 | Jared Morgan has retired himself from the PSC | | Thanks so much for helping create the first set of templates and mentoring Season of Docs. |
| 2021-02-18 | Felicity Brand has retired herself from the PSC | | Thanks so much for doing much of the foundational work in setting up The Good Docs Project. |.
| 2021-02-18 | Nominations for the PSC: Aaron Peters, Aidan Doherty, Ankita Tripathi, Bryan Klein, Daniel Beck, Viraji Ogodapola. Nominations for Committer: Chris Ward, Derek Ardolf. | Passed by private PSC vote | 

